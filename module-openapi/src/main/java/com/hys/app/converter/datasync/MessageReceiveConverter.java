package com.hys.app.converter.datasync;

import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import com.hys.app.model.datasync.dos.MessageReceiveDO;
import com.hys.app.model.datasync.vo.MessageReceiveVO;
import com.hys.app.model.datasync.dto.MessageReceiveDTO;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 消息接收 Convert
 *
 * @author 张崧
 * @since 2023-12-19 17:41:37
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface MessageReceiveConverter {
    MessageReceiveDO convert(MessageReceiveDTO messageReceiveDTO);
    
    MessageReceiveVO convert(MessageReceiveDO messageReceiveDO);
    
    List<MessageReceiveVO> convert(List<MessageReceiveDO> list);
    
    WebPage<MessageReceiveVO> convert(WebPage<MessageReceiveDO> webPage);
    
}

