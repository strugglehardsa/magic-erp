package com.hys.app.model.oauth2.vo;

import com.hys.app.model.base.KeyValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 开放接口 - 授权页的信息 Response VO
 *
 * @author 张崧
 * @since 2024-02-20
 */
@ApiModel(description = "【开放接口】授权页的信息 Response VO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OAuth2OpenAuthorizeInfoRespVO {

    /**
     * 客户端
     */
    private Client client;

    @ApiModelProperty(value = "scope 的选中信息,使用 List 保证有序性，Key 是 scope，Value 为是否选中")
    private List<KeyValue<String, Boolean>> scopes;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Client {

        @ApiModelProperty(value = "应用名")
        private String name;

        @ApiModelProperty(value = "应用图标")
        private String logo;

    }

}
