package com.hys.app.model.oauth2.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * OAuth2 授权类型（模式）的枚举
 *
 * @author 张崧
 * @since 2024-02-18
 */
@AllArgsConstructor
@Getter
public enum OAuth2GrantTypeEnum {

    /**
     * 密码模式
     */
    PASSWORD("password"),
    /**
     * 授权码模式
     */
    AUTHORIZATION_CODE("authorization_code"),
    /**
     * 简化模式
     */
    IMPLICIT("implicit"),
    /**
     * 客户端模式
     */
    CLIENT_CREDENTIALS("client_credentials"),
    /**
     * 刷新模式
     */
    REFRESH_TOKEN("refresh_token"),
    ;

    private final String grantType;

    public static OAuth2GrantTypeEnum getByGranType(String grantType) {
        return Arrays.stream(values()).filter(o -> o.getGrantType().equals(grantType)).findAny().orElse(null);
    }

}
