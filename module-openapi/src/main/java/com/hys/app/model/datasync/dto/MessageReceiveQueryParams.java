package com.hys.app.model.datasync.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 消息接收查询参数
 *
 * @author 张崧
 * @since 2023-12-19 17:41:37
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class MessageReceiveQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "type", value = "消息类型")
    private String type;
    
    @ApiModelProperty(name = "msg_id", value = "外部系统的消息id")
    private String msgId;
    
    @ApiModelProperty(name = "content", value = "消息内容")
    private String content;
    
    @ApiModelProperty(name = "produce_time", value = "外部系统生成消息的时间")
    private Long[] produceTime;
    
    @ApiModelProperty(name = "receive_time", value = "接收消息的时间")
    private Long[] receiveTime;
    
    @ApiModelProperty(name = "status", value = "状态")
    private String status;
    
    @ApiModelProperty(name = "remark", value = "处理备注")
    private String remark;
    
}

