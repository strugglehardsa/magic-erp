package com.hys.app.service.system;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.system.dos.SmsSendRecord;

/**
 * 短信发送记录业务层
 * @author zhangsong
 * @version v1.0
 * @since v1.0
 * 2021-02-05 17:17:30
 */
public interface SmsSendRecordManager {

	/**
	 * 查询短信发送记录列表
	 * @param page 页码
	 * @param pageSize 每页数量
	 * @return WebPage
	 */
     WebPage list(Long page, Long pageSize);
	/**
	 * 添加短信发送记录
	 * @param smsSendRecord 短信发送记录
	 * @return SmsSendRecord 短信发送记录
	 */
	SmsSendRecord add(SmsSendRecord smsSendRecord);
	
	/**
	 * 获取短信发送记录
	 * @param id 短信发送记录主键
	 * @return SmsSendRecord  短信发送记录
	 */
	SmsSendRecord getModel(Long id);


	/**
	 * 阿里短信发送结果回调
	 * @param bizId 回执id
	 * @param isSuccess true:成功
	 */
	void smsSendCallback(String bizId, String isSuccess);
}