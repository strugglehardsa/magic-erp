package com.hys.app.service.system.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.util.PageConvert;
import com.hys.app.framework.util.StringUtil;
import com.hys.app.mapper.system.SystemLogsMapper;
import com.hys.app.model.system.dos.SystemLogs;
import com.hys.app.model.system.dto.SystemLogsParam;
import com.hys.app.service.system.SystemLogsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 系统日志业务类
 *
 * @author fk
 * @version v2.0
 * @since v2.0
 * 2021-03-22 16:05:58
 */
@Service
public class SystemLogsManagerImpl implements SystemLogsManager {

    @Autowired
    private SystemLogsMapper systemLogsMapper;

    @Override
    public WebPage list(SystemLogsParam param, String client) {

        //新建查询条件包装器
        QueryWrapper<SystemLogs> wrapper = new QueryWrapper<>();

        //如果开始时间不为空，则查询操作时间大于等于开始时间的记录
        wrapper.eq("client", client)
                .eq(param.getLevel() != null, "level", param.getLevel())
                .ge(param.getStartTime() != null, "operate_time", param.getStartTime())
                //如果结束时间不为空，则查询操作时间小于等于结束时间的记录
                .le(param.getEndTime() != null, "operate_time", param.getEndTime())
                //如果关键字不为空，则模糊匹配操作者名字，ip,操作内容
                .and(!StringUtil.isEmpty(param.getKeyword()), e -> {
                    e.like("operator_name", param.getKeyword())
                            .or().like("operate_detail", param.getKeyword())
                            .or().like("operate_ip", param.getKeyword());
                })
                //商家id
                .eq(param.getSellerId() != null, "seller_id", param.getSellerId()).orderByDesc("operate_time");


        IPage<SystemLogs> iPage = systemLogsMapper.selectPage(new Page<>(param.getPageNo(), param.getPageSize()), wrapper);

        return PageConvert.convert(iPage);
    }

    @Override
    @Transactional(value = "", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public SystemLogs add(SystemLogs systemLogs) {
        this.systemLogsMapper.insert(systemLogs);

        return systemLogs;
    }

    @Override
    @Transactional(value = "", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete(Long id) {
        this.systemLogsMapper.deleteById(id);
    }

    @Override
    public SystemLogs getModel(Long id) {
        return this.systemLogsMapper.selectById(id);
    }

    @Override
    public void deleteByTime(Long time) {
        QueryWrapper<SystemLogs> wrapper = new QueryWrapper<>();
        wrapper.le("operate_time", time);
        this.systemLogsMapper.delete(wrapper);
    }
}
