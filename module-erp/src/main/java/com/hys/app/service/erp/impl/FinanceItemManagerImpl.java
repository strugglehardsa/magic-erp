package com.hys.app.service.erp.impl;

import com.hys.app.converter.erp.FinanceItemConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseServiceImpl;
import com.hys.app.mapper.erp.FinanceItemMapper;
import com.hys.app.model.erp.dos.FinanceItemDO;
import com.hys.app.model.erp.dto.FinanceItemDTO;
import com.hys.app.model.erp.dto.FinanceItemQueryParams;
import com.hys.app.model.erp.enums.FinanceAmountTypeEnum;
import com.hys.app.model.erp.enums.FinanceExpandTypeEnum;
import com.hys.app.model.erp.enums.FinanceIncomeTypeEnum;
import com.hys.app.model.erp.enums.NoBusinessTypeEnum;
import com.hys.app.model.erp.vo.FinanceItemVO;
import com.hys.app.service.erp.FinanceItemManager;
import com.hys.app.service.erp.NoGenerateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 财务明细业务层实现
 *
 * @author 张崧
 * 2024-03-15 17:40:23
 */
@Service
public class FinanceItemManagerImpl extends BaseServiceImpl<FinanceItemMapper, FinanceItemDO> implements FinanceItemManager {

    @Autowired
    private FinanceItemConverter converter;

    @Autowired
    private NoGenerateManager noGenerateManager;

    @Override
    public WebPage<FinanceItemVO> list(FinanceItemQueryParams queryParams) {
        WebPage<FinanceItemDO> page = baseMapper.selectPage(queryParams);
        return converter.convertPage(page);
    }

    @Override
    public FinanceItemVO getDetail(Long id) {
        FinanceItemDO financeItemDO = getById(id);
        return converter.convert(financeItemDO);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        removeBatchByIds(ids);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addIncome(FinanceIncomeTypeEnum type, String sourceSn, Double price) {
        this.add(new FinanceItemDTO(type.name(), "单号：" + sourceSn, price, FinanceAmountTypeEnum.Income));
    }

    @Override
    public void addExpand(FinanceExpandTypeEnum type, String sourceSn, Double price) {
        this.add(new FinanceItemDTO(type.name(), "单号：" + sourceSn, price, FinanceAmountTypeEnum.Expend));
    }

    @Override
    public void add(FinanceItemDTO financeItemDTO) {
        FinanceItemDO financeItemDO = converter.convert(financeItemDTO);
        financeItemDO.setSn(noGenerateManager.generate(NoBusinessTypeEnum.FinanceItem));
        save(financeItemDO);
    }

}

