package com.hys.app.service.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.database.mybatisplus.base.BaseService;
import com.hys.app.model.erp.dos.OrderItemDO;
import com.hys.app.model.erp.dto.OrderItemQueryParams;
import com.hys.app.model.erp.vo.OrderItemVO;

import java.util.Collection;
import java.util.List;

/**
 * 订单明细业务层接口
 *
 * @author 张崧
 * 2024-01-24 16:39:39
 */
public interface OrderItemManager extends BaseService<OrderItemDO> {
    /**
     * 分页列表
     *
     * @param queryParams 查询条件
     * @return 分页数据
     */
    WebPage<OrderItemVO> list(OrderItemQueryParams queryParams);

    /**
     * 查询详情
     *
     * @param id 主键
     * @return 详情数据
     */
    OrderItemVO getDetail(Long id);

    /**
     * 删除订单明细
     *
     * @param orderId
     */
    void deleteByOrderId(Long orderId);

    /**
     * 根据订单id查询
     *
     * @param orderIds
     * @return
     */
    List<OrderItemDO> listByOrderIds(Collection<Long> orderIds);

    /**
     * 更新订单项的已退货数量
     * @param orderItemId
     * @param returnNum
     */
    void increaseReturnNum(Long orderItemId, Integer returnNum);
}

