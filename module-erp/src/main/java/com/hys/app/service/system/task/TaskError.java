package com.hys.app.service.system.task;

@FunctionalInterface
public interface TaskError {

    void onError(Throwable throwable, String errorMsg);
}
