package com.hys.app.service.erp.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.exception.ServiceException;
import com.hys.app.framework.util.BeanUtil;
import com.hys.app.framework.util.DateUtil;
import com.hys.app.framework.util.PageConvert;
import com.hys.app.framework.util.StringUtil;
import com.hys.app.mapper.erp.ProcurementPlanMapper;
import com.hys.app.model.erp.dos.ProcurementPlan;
import com.hys.app.model.erp.dos.ProcurementPlanProduct;
import com.hys.app.model.erp.dto.ProcurementPlanDTO;
import com.hys.app.model.erp.dto.ProcurementPlanQueryParam;
import com.hys.app.model.erp.enums.NoBusinessTypeEnum;
import com.hys.app.model.erp.vo.ProcurementPlanVO;
import com.hys.app.service.erp.NoGenerateManager;
import com.hys.app.service.erp.ProcurementPlanProductManager;
import com.hys.app.service.erp.ProcurementPlanManager;
import com.hys.app.service.erp.WarehouseEntryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 采购计划业务接口实现类
 * @author dmy
 * 2023-12-05
 */
@Service
public class ProcurementPlanManagerImpl extends ServiceImpl<ProcurementPlanMapper, ProcurementPlan> implements ProcurementPlanManager {

    @Autowired
    private ProcurementPlanProductManager procurementPlanProductManager;

    @Autowired
    private NoGenerateManager noGenerateManager;

    @Autowired
    private WarehouseEntryManager warehouseEntryManager;

    /**
     * 查询采购计划分页列表数据
     *
     * @param params 查询参数
     * @return 分页数据
     */
    @Override
    public WebPage list(ProcurementPlanQueryParam params) {
        IPage<ProcurementPlan> iPage = this.lambdaQuery()
                .like(StringUtil.notEmpty(params.getKeyword()), ProcurementPlan::getSn, params.getKeyword())
                .like(StringUtil.notEmpty(params.getSn()), ProcurementPlan::getSn, params.getSn())
                .eq(params.getDeptId() != null, ProcurementPlan::getDeptId, params.getDeptId())
                .gt(params.getStartTime() != null, ProcurementPlan::getFormationTime, params.getStartTime())
                .lt(params.getEndTime() != null, ProcurementPlan::getFormationTime, params.getEndTime())
                .eq(params.getFormationPersonId() != null, ProcurementPlan::getFormationPersonId, params.getFormationPersonId())
                .orderByDesc(ProcurementPlan::getCreateTime)
                .page(new Page<>(params.getPageNo(), params.getPageSize()));
        return PageConvert.convert(iPage);
    }

    /**
     * 新增采购计划
     *
     * @param procurementPlanDTO
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void add(ProcurementPlanDTO procurementPlanDTO) {
        //采购计划信息入库
        ProcurementPlan plan = new ProcurementPlan();
        BeanUtil.copyProperties(procurementPlanDTO, plan);
        //生成采购计划编号
        String sn = this.noGenerateManager.generate(NoBusinessTypeEnum.ProcurementPlan);
        plan.setSn(sn);
        //设置创建时间
        plan.setCreateTime(DateUtil.getDateline());
        this.save(plan);
        //获取采购计划ID
        Long id = plan.getId();
        //入库采购计划商品
        this.procurementPlanProductManager.saveProduct(id, procurementPlanDTO.getProductList());
    }

    /**
     * 编辑采购计划
     *
     * @param id 主键ID
     * @param procurementPlanDTO 采购计划信息
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void edit(Long id, ProcurementPlanDTO procurementPlanDTO) {
        //获取采购计划旧数据
        ProcurementPlan plan  = this.getById(id);
        //复制修改后的数据
        BeanUtil.copyProperties(procurementPlanDTO, plan);
        //修改采购计划信息
        this.updateById(plan);
        //入库采购计划商品
        this.procurementPlanProductManager.saveProduct(id, procurementPlanDTO.getProductList());
    }

    /**
     * 删除采购计划
     * @param ids 采购计划主键ID集合
     */
    @Override
    @Transactional(value = "goodsTransactionManager", propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete(List<Long> ids) {
        //判断采购计划是否已被入库单占用
        boolean isUsed = this.warehouseEntryManager.checkPlanIsUsed(ids);
        if (isUsed) {
            throw new ServiceException("删除的采购计划中存在被入库单占用的，无法删除");
        }
        //批量删除采购计划
        this.removeByIds(ids);
        //删除采购计划商品信息
        this.procurementPlanProductManager.deleteProduct(ids);
    }

    /**
     * 根据编号获取采购计划信息
     *
     * @param id 主键ID
     * @return
     */
    @Override
    public ProcurementPlanVO getDetail(Long id) {
        ProcurementPlan plan = this.getById(id);
        ProcurementPlanVO planVO = new ProcurementPlanVO();
        BeanUtil.copyProperties(plan, planVO);

        //获取采购计划商品信息
        List<ProcurementPlanProduct> goodsList = this.procurementPlanProductManager.list(id);
        planVO.setProductList(goodsList);
        return planVO;
    }

    /**
     * 获取所有采购计划信息
     *
     * @return
     */
    @Override
    public List<ProcurementPlan> listAll() {
        return this.lambdaQuery().list();
    }
}
