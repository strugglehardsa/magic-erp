package com.hys.app.service.erp;

import com.hys.app.model.erp.dos.ProcurementPlanProduct;
import com.hys.app.model.erp.dto.ProcurementPlanProductDTO;

import java.util.List;

/**
 * 采购计划产品业务接口
 * @author dmy
 * 2023-12-05
 */
public interface ProcurementPlanProductManager {

    /**
     * 新增采购计划产品信息
     *
     * @param planId 采购计划ID
     * @param productList 产品信息集合
     */
    void saveProduct(Long planId, List<ProcurementPlanProductDTO> productList);

    /**
     * 删除采购计划产品信息
     *
     * @param planIds 采购计划ID集合
     */
    void deleteProduct(List<Long> planIds);

    /**
     * 根据采购计划ID获取采购计划产品信息集合
     * @param planId 采购计划ID
     * @return
     */
    List<ProcurementPlanProduct> list(Long planId);
}
