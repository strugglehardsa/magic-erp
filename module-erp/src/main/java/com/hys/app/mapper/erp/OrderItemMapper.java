package com.hys.app.mapper.erp;

import org.apache.ibatis.annotations.Mapper;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.OrderItemDO;
import com.hys.app.model.erp.dto.OrderItemQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 订单明细 Mapper
 *
 * @author 张崧
 * 2024-01-24 16:39:38
 */
@Mapper
public interface OrderItemMapper extends BaseMapperX<OrderItemDO> {

    default WebPage<OrderItemDO> selectPage(OrderItemQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(OrderItemDO::getOrderId, params.getOrderId())
                .eqIfPresent(OrderItemDO::getGoodsId, params.getGoodsId())
                .eqIfPresent(OrderItemDO::getProductId, params.getProductId())
                .eqIfPresent(OrderItemDO::getProductSn, params.getProductSn())
                .eqIfPresent(OrderItemDO::getProductName, params.getProductName())
                .eqIfPresent(OrderItemDO::getProductSpecification, params.getProductSpecification())
                .eqIfPresent(OrderItemDO::getProductUnit, params.getProductUnit())
                .eqIfPresent(OrderItemDO::getCategoryId, params.getCategoryId())
                .eqIfPresent(OrderItemDO::getCategoryName, params.getCategoryName())
                .eqIfPresent(OrderItemDO::getRemark, params.getRemark())
                .orderByDesc(OrderItemDO::getId)
                .page(params);
    }

}

