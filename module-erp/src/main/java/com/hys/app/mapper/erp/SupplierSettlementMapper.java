package com.hys.app.mapper.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.SupplierSettlementDO;
import com.hys.app.model.erp.dto.SupplierSettlementQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 供应商结算单的Mapper
 *
 * @author 张崧
 * @since 2023-12-15 14:09:09
 */
public interface SupplierSettlementMapper extends BaseMapperX<SupplierSettlementDO> {

    default WebPage<SupplierSettlementDO> selectPage(SupplierSettlementQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(SupplierSettlementDO::getSn, params.getSn())
                .eqIfPresent(SupplierSettlementDO::getSupplierId, params.getSupplierId())
                .geIfPresent(SupplierSettlementDO::getCreateTime, params.getStartTime())
                .leIfPresent(SupplierSettlementDO::getCreateTime, params.getEndTime())
                .eqIfPresent(SupplierSettlementDO::getTotalPrice, params.getTotalPrice())
                .orderByDesc(SupplierSettlementDO::getId)
                .page(params);
    }

}

