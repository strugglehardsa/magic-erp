package com.hys.app.mapper.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.OrderReturnItemDO;
import com.hys.app.model.erp.dto.OrderReturnItemQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 订单退货项的Mapper
 *
 * @author 张崧
 * @since 2023-12-14 15:42:07
 */
public interface OrderReturnItemMapper extends BaseMapperX<OrderReturnItemDO> {

    default WebPage<OrderReturnItemDO> selectPage(OrderReturnItemQueryParams params) {
        return lambdaQuery()
                .eqIfPresent(OrderReturnItemDO::getOrderReturnId, params.getOrderReturnId())
                .eqIfPresent(OrderReturnItemDO::getWarehouseEntrySn, params.getWarehouseEntrySn())
                .eqIfPresent(OrderReturnItemDO::getWarehouseEntryBatchId, params.getWarehouseEntryBatchId())
                .eqIfPresent(OrderReturnItemDO::getWarehouseEntryBatchSn, params.getWarehouseEntryBatchSn())
                .eqIfPresent(OrderReturnItemDO::getProductId, params.getProductId())
                .eqIfPresent(OrderReturnItemDO::getProductSn, params.getProductSn())
                .eqIfPresent(OrderReturnItemDO::getProductName, params.getProductName())
                .eqIfPresent(OrderReturnItemDO::getProductSpecification, params.getProductSpecification())
                .eqIfPresent(OrderReturnItemDO::getProductUnit, params.getProductUnit())
                .eqIfPresent(OrderReturnItemDO::getProductPrice, params.getProductPrice())
                .eqIfPresent(OrderReturnItemDO::getOrderNum, params.getOrderNum())
                .eqIfPresent(OrderReturnItemDO::getReturnNum, params.getReturnNum())
                .orderByDesc(OrderReturnItemDO::getId)
                .page(params);
    }

}

