package com.hys.app.mapper.erp;

import org.apache.ibatis.annotations.Mapper;
import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.erp.dos.EnterpriseDO;
import com.hys.app.model.erp.dto.EnterpriseQueryParams;
import com.hys.app.framework.database.WebPage;

/**
 * 企业 Mapper
 *
 * @author 张崧
 * 2024-03-25 15:44:10
 */
@Mapper
public interface EnterpriseMapper extends BaseMapperX<EnterpriseDO> {

    default WebPage<EnterpriseDO> selectPage(EnterpriseQueryParams params) {
        return lambdaQuery()
                .likeIfPresent(EnterpriseDO::getName, params.getName())
                .likeIfPresent(EnterpriseDO::getSn, params.getSn())
                .likeIfPresent(EnterpriseDO::getMobile, params.getMobile())
                .likeIfPresent(EnterpriseDO::getEmail, params.getEmail())
                .eqIfPresent(EnterpriseDO::getZipCode, params.getZipCode())
                .likeIfPresent(EnterpriseDO::getTaxNum, params.getTaxNum())
                .eqIfPresent(EnterpriseDO::getBankName, params.getBankName())
                .eqIfPresent(EnterpriseDO::getAddress, params.getAddress())
                .eqIfPresent(EnterpriseDO::getLinkman, params.getLinkman())
                .eqIfPresent(EnterpriseDO::getLinkPhone, params.getLinkPhone())
                .eqIfPresent(EnterpriseDO::getFax, params.getFax())
                .eqIfPresent(EnterpriseDO::getBankAccount, params.getBankAccount())
                .eqIfPresent(EnterpriseDO::getBankAccountName, params.getBankAccountName())
                .eqIfPresent(EnterpriseDO::getRemark, params.getRemark())
                .betweenIfPresent(BaseDO::getCreateTime, params.getCreateTime())
                .orderByDesc(EnterpriseDO::getId)
                .page(params);
    }

}

