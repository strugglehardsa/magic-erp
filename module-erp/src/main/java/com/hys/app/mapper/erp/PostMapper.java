package com.hys.app.mapper.erp;

import com.hys.app.framework.database.mybatisplus.base.BaseMapperX;
import com.hys.app.model.erp.dos.PostDO;
import com.hys.app.model.erp.dto.PostQueryParams;
import com.hys.app.framework.database.WebPage;

import java.util.List;

/**
 * 岗位的Mapper
 *
 * @author 张崧
 * 2023-11-29 17:15:22
 */
public interface PostMapper extends BaseMapperX<PostDO> {

    default WebPage<PostDO> selectPage(PostQueryParams params) {
        return lambdaQuery()
                .likeIfPresent(PostDO::getName, params.getKeyword())
                .likeIfPresent(PostDO::getName, params.getName())
                .likeIfPresent(PostDO::getSn, params.getSn())
                .likeIfPresent(PostDO::getDescription, params.getDescription())
                .likeIfPresent(PostDO::getMembers, params.getMembers())
                .orderByAsc(PostDO::getSort)
                .page(params);
    }

    default List<PostDO> listAll(){
        return lambdaQuery().orderByAsc(PostDO::getSort).list();
    }
}

