package com.hys.app.controller.erp.goods;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.GoodsLabelDTO;
import com.hys.app.model.erp.dto.GoodsLabelQueryParams;
import com.hys.app.model.erp.vo.GoodsLabelVO;
import com.hys.app.service.erp.GoodsLabelManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * 商品标签 API
 *
 * @author 张崧
 * 2024-03-20 16:23:19
 */
@RestController
@RequestMapping("/admin/erp/goodsLabel")
@Api(tags = "商品标签API")
@Validated
public class GoodsLabelController {

    @Autowired
    private GoodsLabelManager goodsLabelManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<GoodsLabelVO> list(GoodsLabelQueryParams queryParams) {
        return goodsLabelManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    public void add(@RequestBody @Valid GoodsLabelDTO goodsLabelDTO) {
        goodsLabelManager.add(goodsLabelDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    public void edit(@PathVariable Long id, @RequestBody @Valid GoodsLabelDTO goodsLabelDTO) {
        goodsLabelDTO.setId(id);
        goodsLabelManager.edit(goodsLabelDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public GoodsLabelVO getDetail(@PathVariable Long id) {
        return goodsLabelManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    public void delete(@PathVariable List<Long> ids) {
        goodsLabelManager.delete(ids);
    }

}

