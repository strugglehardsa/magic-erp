package com.hys.app.controller.system;

import com.hys.app.converter.system.DictTypeConverter;
import com.hys.app.framework.database.WebPage;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.model.system.dos.DictTypeDO;
import com.hys.app.model.system.dto.DictTypeCreateDTO;
import com.hys.app.model.system.dto.DictTypeQueryParams;
import com.hys.app.model.system.dto.DictTypeUpdateDTO;
import com.hys.app.model.system.vo.DictTypeRespVO;
import com.hys.app.model.system.vo.DictTypeSimpleRespVO;
import com.hys.app.service.system.DictTypeManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "管理后台 - 字典类型")
@RestController
@RequestMapping("/admin/systems/dict-type")
@Validated
public class DictTypeController {

    @Resource
    private DictTypeManager dictTypeManager;

    @Autowired
    private DictTypeConverter converter;

    @PostMapping("/create")
    @ApiOperation(value = "创建字典类型")
    @Log(client = LogClient.admin, detail = "新增名称为[${reqVO.name}]的字典类型")
    public void createDictType(@Valid @RequestBody DictTypeCreateDTO reqVO) {
        dictTypeManager.createDictType(reqVO);
    }

    @PutMapping("/update")
    @ApiOperation(value = "修改字典类型")
    @Log(client = LogClient.admin, detail = "修改名称为[${reqVO.name}]的字典类型")
    public void updateDictType(@Valid @RequestBody DictTypeUpdateDTO reqVO) {
        dictTypeManager.updateDictType(reqVO);
    }

    @DeleteMapping("/delete")
    @ApiOperation(value = "删除字典类型")
    @ApiImplicitParam(name = "id", value = "编号", required = true, example = "1024")
    @Log(client = LogClient.admin, detail = "删除ID为[${id}]的字典类型")
    public void deleteDictType(Long id) {
        dictTypeManager.deleteDictType(id);
    }

    @ApiOperation(value = "/获得字典类型的分页列表")
    @GetMapping("/page")
    public WebPage<DictTypeRespVO> pageDictTypes(@Valid DictTypeQueryParams reqVO) {
        WebPage<DictTypeDO> dictTypePage = dictTypeManager.getDictTypePage(reqVO);
        return converter.convertPage(dictTypePage);
    }

    @ApiOperation(value = "/查询字典类型详细")
    @ApiImplicitParam(name = "id", value = "编号", required = true, example = "1024")
    @GetMapping(value = "/get")
    public DictTypeRespVO getDictType(@RequestParam("id") Long id) {
        DictTypeDO dictTypeDO = dictTypeManager.getDictType(id);
        return converter.convert(dictTypeDO);
    }

    @GetMapping("/list-all-simple")
    @ApiOperation(value = "获得全部字典类型列表", notes = "包括开启 + 禁用的字典类型，主要用于前端的下拉选项")
    public List<DictTypeSimpleRespVO> getSimpleDictTypeList() {
        List<DictTypeDO> list = dictTypeManager.getDictTypeList();
        return converter.convertList(list);
    }

}
