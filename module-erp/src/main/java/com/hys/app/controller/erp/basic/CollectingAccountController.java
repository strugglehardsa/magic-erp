package com.hys.app.controller.erp.basic;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dto.CollectingAccountDTO;
import com.hys.app.model.erp.dto.CollectingAccountQueryParams;
import com.hys.app.model.erp.vo.CollectingAccountVO;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.service.erp.CollectingAccountManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 收款账户 API
 *
 * @author 张崧
 * 2024-01-24 14:43:18
 */
@RestController
@RequestMapping("/admin/erp/collectingAccount")
@Api(tags = "收款账户API")
@Validated
public class CollectingAccountController {

    @Autowired
    private CollectingAccountManager collectingAccountManager;

    @ApiOperation(value = "分页列表")
    @GetMapping
    public WebPage<CollectingAccountVO> list(CollectingAccountQueryParams queryParams) {
        return collectingAccountManager.list(queryParams);
    }

    @ApiOperation(value = "添加")
    @PostMapping
    @Log(client = LogClient.admin, detail = "新增收款账户")
    public void add(@RequestBody @Valid CollectingAccountDTO collectingAccountDTO) {
        collectingAccountManager.add(collectingAccountDTO);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/{id}")
    @Log(client = LogClient.admin, detail = "修改id为[${id}]的收款账户")
    public void edit(@PathVariable Long id, @RequestBody @Valid CollectingAccountDTO collectingAccountDTO) {
        collectingAccountDTO.setId(id);
        collectingAccountManager.edit(collectingAccountDTO);
    }

    @ApiOperation(value = "查询")
    @GetMapping("/{id}")
    public CollectingAccountVO getDetail(@PathVariable Long id) {
        return collectingAccountManager.getDetail(id);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/{ids}")
    @Log(client = LogClient.admin, detail = "删除id为[${ids}]的收款账户")
    public void delete(@PathVariable List<Long> ids) {
        collectingAccountManager.delete(ids);
    }

    @ApiOperation(value = "设为默认")
    @PutMapping("/{id}/default")
    @Log(client = LogClient.admin, detail = "将id为[${id}]的收款账户设为默认")
    public void setDefault(@PathVariable Long id) {
        collectingAccountManager.setDefault(id);
    }

    @ApiOperation(value = "启用/禁用")
    @PutMapping("/{id}/enableFlag")
    @ApiImplicitParam(name = "enable_flag", value = "true启用 false禁用", required = true, paramType = "query")
    @Log(client = LogClient.admin, detail = "修改id为[${id}]的收款账户的启用/禁用状态")
    public void updateEnableFlag(@PathVariable Long id, @NotNull(message = "状态不能为空") Boolean enableFlag) {
        collectingAccountManager.updateEnableFlag(id, enableFlag);
    }

}

