package com.hys.app.controller.system;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.support.LogClient;
import com.hys.app.model.support.validator.annotation.Log;
import com.hys.app.model.system.dos.AdminUser;
import com.hys.app.model.system.vo.AdminUserVO;
import com.hys.app.service.system.AdminUserManager;
import com.hys.app.service.system.DeptManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

/**
 * 平台管理员控制器
 *
 * @author zh
 * @version v7.0
 * @since v7.0.0
 * 2018-06-20 20:38:26
 */
@RestController
@RequestMapping("/admin/systems/manager/admin-users")
@Api(description = "平台管理员管理相关API")
@Validated
public class AdminUserOperationManagerController {

    @Autowired
    private AdminUserManager adminUserManager;

    @Autowired
    private DeptManager deptManager;


    @ApiOperation(value = "查询平台管理员列表", response = AdminUser.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page_no", value = "页码", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page_size", value = "每页显示数量", required = true, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "user_state", value = "是否删除,0为正常,-1为删除状态", dataType = "String", paramType = "query"),
    })
    @GetMapping
    public WebPage list(@ApiIgnore Long pageNo, @ApiIgnore Long pageSize, @ApiIgnore String keyword, @ApiIgnore Integer userState) {
        return this.adminUserManager.list(pageNo, pageSize, keyword, userState);
    }


    @ApiOperation(value = "添加平台管理员", response = AdminUser.class)
    @PostMapping
    @Log(client = LogClient.admin, detail = "添加用户编号为[${adminUserVO.sn}]的管理员")
    public AdminUser add(@Valid AdminUserVO adminUserVO) {
        return this.adminUserManager.add(adminUserVO);
    }

    @PutMapping(value = "/{id}")
    @ApiOperation(value = "修改平台管理员", response = AdminUser.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", required = true, dataType = "int", paramType = "path")
    })
    @Log(client = LogClient.admin, detail = "修改ID为[${id}]的管理员信息")
    public AdminUser edit(@Valid AdminUserVO adminUserVO, @PathVariable Long id) {
        return this.adminUserManager.edit(adminUserVO, id);
    }


    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "删除平台管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要删除的平台管理员主键", required = true, dataType = "int", paramType = "path")
    })
    @Log(client = LogClient.admin, detail = "删除ID为[${id}]的管理员信息")
    public String delete(@PathVariable Long id) {
        this.adminUserManager.delete(id);
        return "";
    }

    @PutMapping(value = "/{id}/recover")
    @ApiOperation(value = "恢复平台管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "平台管理员主键", required = true, dataType = "int", paramType = "path")
    })
    @Log(client = LogClient.admin, detail = "恢复ID为[${id}]的管理员信息")
    public void recover(@PathVariable Long id) {
        this.adminUserManager.recover(id);
    }


    @GetMapping(value = "/{id}")
    @ApiOperation(value = "查询一个平台管理员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "要查询的平台管理员主键", required = true, dataType = "int", paramType = "path")
    })
    public AdminUser get(@PathVariable Long id) {

        AdminUser adminUser = this.adminUserManager.getModel(id);

        return adminUser;
    }
}
