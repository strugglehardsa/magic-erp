package com.hys.app.converter.erp;

import org.mapstruct.Mapper;
import com.hys.app.model.erp.dos.StockBatchFlowDO;
import com.hys.app.model.erp.vo.StockBatchFlowVO;
import com.hys.app.model.erp.dto.StockBatchFlowDTO;
import com.hys.app.model.erp.vo.StockBatchFlowExcelVO;
import org.mapstruct.MappingConstants;
import com.hys.app.framework.database.WebPage;
import java.util.List;

/**
 * 库存批次流水 Converter
 *
 * @author 张崧
 * 2024-01-16 11:39:01
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface StockBatchFlowConverter {
    
    StockBatchFlowDO convert(StockBatchFlowDTO stockBatchFlowDTO);
    
    StockBatchFlowVO convert(StockBatchFlowDO stockBatchFlowDO);
    
    List<StockBatchFlowVO> convertList(List<StockBatchFlowDO> list);
    
    List<StockBatchFlowExcelVO> convertExcel(List<StockBatchFlowDO> list);
    
    WebPage<StockBatchFlowVO> convertPage(WebPage<StockBatchFlowDO> webPage);
    
}

