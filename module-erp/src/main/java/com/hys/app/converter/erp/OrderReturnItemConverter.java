package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.util.DateUtil;
import com.hys.app.model.erp.dos.*;
import com.hys.app.model.erp.dto.OrderReturnDTO;
import com.hys.app.model.erp.dto.OrderReturnItemDTO;
import com.hys.app.model.erp.enums.NoBusinessTypeEnum;
import com.hys.app.model.erp.enums.OrderTypeEnum;
import com.hys.app.model.erp.vo.OrderReturnItemVO;
import com.hys.app.service.erp.NoGenerateManager;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 订单退货项 Convert
 *
 * @author 张崧
 * @since 2023-12-14 15:42:07
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface OrderReturnItemConverter {
    OrderReturnItemDO convert(OrderReturnItemDTO orderReturnItemDTO);

    OrderReturnItemVO convert(OrderReturnItemDO orderReturnItemDO);

    List<OrderReturnItemVO> convert(List<OrderReturnItemDO> list);

    WebPage<OrderReturnItemVO> convert(WebPage<OrderReturnItemDO> webPage);

    default List<OrderReturnItemDO> combination(Long orderReturnId, OrderReturnDTO orderReturnDTO) {
        List<OrderReturnItemDTO> itemList = orderReturnDTO.getItemList();
        boolean isToC = orderReturnDTO.getOrderDO().getType() == OrderTypeEnum.TO_C;
        return itemList.stream().map(orderReturnItemDTO -> {
            if (isToC) {
                OrderItemDO orderItemDO = orderReturnDTO.getOrderItemMap().get(orderReturnItemDTO.getOrderItemId());
                WarehouseEntryBatchDO batchDO = orderReturnDTO.getBatchMap().get(orderItemDO.getBatchId());
                return convert(orderReturnItemDTO, orderItemDO, batchDO, orderReturnId);
            } else {
                WarehouseOutItemDO warehouseOutItemDO = orderReturnDTO.getWarehouseOutItemMap().get(orderReturnItemDTO.getWarehouseOutItemId());
                return convert(warehouseOutItemDO, orderReturnItemDTO, orderReturnId);
            }
        }).collect(Collectors.toList());
    }

    default OrderReturnItemDO convert(OrderReturnItemDTO orderReturnItemDTO, OrderItemDO orderItemDO, WarehouseEntryBatchDO batchDO, Long orderReturnId) {
        OrderReturnItemDO orderReturnItemDO = new OrderReturnItemDO();

        orderReturnItemDO.setOrderReturnId(orderReturnId);
        orderReturnItemDO.setWarehouseEntryId(batchDO.getWarehouseEntryId());
        orderReturnItemDO.setWarehouseEntrySn(batchDO.getWarehouseEntrySn());
        orderReturnItemDO.setWarehouseEntryItemId(batchDO.getWarehouseEntryItemId());
        orderReturnItemDO.setWarehouseEntryBatchId(orderItemDO.getBatchId());
        orderReturnItemDO.setWarehouseEntryBatchSn(orderItemDO.getBatchSn());
        orderReturnItemDO.setOrderItemId(orderItemDO.getId());
        orderReturnItemDO.setGoodsId(orderItemDO.getGoodsId());
        orderReturnItemDO.setProductId(orderItemDO.getProductId());
        orderReturnItemDO.setProductSn(orderItemDO.getProductSn());
        orderReturnItemDO.setProductName(orderItemDO.getProductName());
        orderReturnItemDO.setProductSpecification(orderItemDO.getProductSpecification());
        orderReturnItemDO.setProductUnit(orderItemDO.getProductUnit());
        orderReturnItemDO.setCategoryId(orderItemDO.getCategoryId());
        orderReturnItemDO.setCategoryName(orderItemDO.getCategoryName());
        orderReturnItemDO.setProductPrice(orderItemDO.getPrice());
        orderReturnItemDO.setProductCostPrice(batchDO.getProductCostPrice());
        orderReturnItemDO.setTaxRate(orderItemDO.getTaxRate());
        orderReturnItemDO.setOrderNum(orderItemDO.getNum());
        orderReturnItemDO.setReturnNum(orderReturnItemDTO.getReturnNum());

        return orderReturnItemDO;
    }

    default OrderReturnItemDO convert(WarehouseOutItemDO warehouseOutItemDO, OrderReturnItemDTO orderReturnItemDTO, Long orderReturnId) {
        OrderReturnItemDO orderReturnItemDO = new OrderReturnItemDO();

        orderReturnItemDO.setOrderReturnId(orderReturnId);
        orderReturnItemDO.setWarehouseEntryId(warehouseOutItemDO.getWarehouseEntryId());
        orderReturnItemDO.setWarehouseEntrySn(warehouseOutItemDO.getWarehouseEntrySn());
        orderReturnItemDO.setWarehouseEntryItemId(warehouseOutItemDO.getWarehouseEntryItemId());
        orderReturnItemDO.setWarehouseEntryBatchId(warehouseOutItemDO.getWarehouseEntryBatchId());
        orderReturnItemDO.setWarehouseEntryBatchSn(warehouseOutItemDO.getWarehouseEntryBatchSn());
        orderReturnItemDO.setWarehouseOutItemId(warehouseOutItemDO.getId());
        orderReturnItemDO.setGoodsId(warehouseOutItemDO.getGoodsId());
        orderReturnItemDO.setProductId(warehouseOutItemDO.getProductId());
        orderReturnItemDO.setProductSn(warehouseOutItemDO.getProductSn());
        orderReturnItemDO.setProductName(warehouseOutItemDO.getProductName());
        orderReturnItemDO.setProductSpecification(warehouseOutItemDO.getProductSpecification());
        orderReturnItemDO.setProductUnit(warehouseOutItemDO.getProductUnit());
        orderReturnItemDO.setProductBarcode(warehouseOutItemDO.getProductBarcode());
        orderReturnItemDO.setCategoryId(warehouseOutItemDO.getCategoryId());
        orderReturnItemDO.setCategoryName(warehouseOutItemDO.getCategoryName());
        orderReturnItemDO.setProductPrice(warehouseOutItemDO.getProductPrice());
        orderReturnItemDO.setProductCostPrice(warehouseOutItemDO.getProductCostPrice());
        orderReturnItemDO.setTaxRate(warehouseOutItemDO.getTaxRate());
        orderReturnItemDO.setOrderNum(warehouseOutItemDO.getOutNum());
        orderReturnItemDO.setReturnNum(orderReturnItemDTO.getReturnNum());

        return orderReturnItemDO;
    }

    default List<WarehouseEntryBatchDO> combination(List<OrderReturnItemDO> returnItemList, OrderReturnDO orderReturnDO, NoGenerateManager noGenerateManager) {
        long dateline = DateUtil.getDateline();

        return returnItemList.stream().map(orderReturnItemDO -> {
            WarehouseEntryBatchDO batchDO = new WarehouseEntryBatchDO();

            batchDO.setWarehouseEntryId(orderReturnItemDO.getWarehouseEntryId());
            batchDO.setWarehouseEntrySn(orderReturnItemDO.getWarehouseEntrySn());
            batchDO.setWarehouseEntryItemId(orderReturnItemDO.getWarehouseEntryItemId());
            batchDO.setSn(noGenerateManager.generate(NoBusinessTypeEnum.WarehouseEntryBatch, orderReturnDO.getDeptId()));
            batchDO.setWarehouseId(orderReturnDO.getWarehouseId());
            batchDO.setGoodsId(orderReturnItemDO.getGoodsId());
            batchDO.setProductId(orderReturnItemDO.getProductId());
            batchDO.setProductSn(orderReturnItemDO.getProductSn());
            batchDO.setProductName(orderReturnItemDO.getProductName());
            batchDO.setProductSpecification(orderReturnItemDO.getProductSpecification());
            batchDO.setProductUnit(orderReturnItemDO.getProductUnit());
            batchDO.setProductBarcode(orderReturnItemDO.getProductBarcode());
            batchDO.setCategoryId(orderReturnItemDO.getCategoryId());
            batchDO.setCategoryName(orderReturnItemDO.getCategoryName());
            batchDO.setEntryNum(orderReturnItemDO.getReturnNum());
            batchDO.setEntryPrice(orderReturnItemDO.getProductPrice());
            batchDO.setProductCostPrice(orderReturnItemDO.getProductCostPrice());
            batchDO.setTaxRate(orderReturnItemDO.getTaxRate());
            batchDO.setRemainNum(orderReturnItemDO.getReturnNum());
            batchDO.setEntryTime(dateline);

            return batchDO;
        }).collect(Collectors.toList());
    }
}

