package com.hys.app.converter.erp;

import com.hys.app.framework.util.CurrencyUtil;
import com.hys.app.model.erp.dos.ProcurementContractProduct;
import com.hys.app.model.erp.dos.ProductDO;
import com.hys.app.model.erp.dto.WarehouseEntryProductDTO;
import com.hys.app.model.goods.dos.CategoryDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.Map;

/**
 * @Author chushuai
 * @Date 2024/3/15 15:17
 * @Version 1.0
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ProcurementContractProductConverter {

    default ProcurementContractProduct convert(ProductDO productDO, Map<Long, CategoryDO> categoryMap, Map<Long, WarehouseEntryProductDTO> warehouseEntryProductDTOMap){
        ProcurementContractProduct procurementContractProduct = new ProcurementContractProduct();
        procurementContractProduct.setProductId(productDO.getId());
        procurementContractProduct.setSpecification(productDO.getSpecification());
        procurementContractProduct.setProductSn(productDO.getSn());
        procurementContractProduct.setProductName(productDO.getName());
        procurementContractProduct.setCategoryName(categoryMap.get(productDO.getCategoryId()).getName());
        procurementContractProduct.setCategoryId(productDO.getCategoryId());
        procurementContractProduct.setGoodsId(productDO.getGoodsId());
        procurementContractProduct.setSpecification(productDO.getSpecification());
        procurementContractProduct.setUnit(productDO.getUnit());
        WarehouseEntryProductDTO warehouseEntryProductDTO = warehouseEntryProductDTOMap.get(productDO.getId());
        procurementContractProduct.setStockNum(warehouseEntryProductDTO.getNum());
        procurementContractProduct.setPrice(warehouseEntryProductDTO.getPrice());
        procurementContractProduct.setTaxRate(warehouseEntryProductDTO.getTaxRate());
        procurementContractProduct.setTotalPrice(CurrencyUtil.mul(warehouseEntryProductDTO.getNum(), warehouseEntryProductDTO.getPrice()));

        return procurementContractProduct;
    }
}
