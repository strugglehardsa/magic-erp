package com.hys.app.converter.erp;

import com.hys.app.framework.util.CurrencyUtil;
import com.hys.app.model.erp.dos.WarehouseEntryDO;
import com.hys.app.model.erp.dos.WarehouseEntryProductDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import com.hys.app.model.erp.dos.SupplierSettlementItemDO;
import com.hys.app.model.erp.vo.SupplierSettlementItemVO;
import com.hys.app.model.erp.dto.SupplierSettlementItemDTO;
import com.hys.app.framework.database.WebPage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 供应商结算单明细 Convert
 *
 * @author 张崧
 * @since 2023-12-15 14:09:20
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface SupplierSettlementItemConverter {

    SupplierSettlementItemDO convert(SupplierSettlementItemDTO supplierSettlementItemDTO);
    
    SupplierSettlementItemVO convert(SupplierSettlementItemDO supplierSettlementItemDO);
    
    List<SupplierSettlementItemVO> convert(List<SupplierSettlementItemDO> list);
    
    WebPage<SupplierSettlementItemVO> convert(WebPage<SupplierSettlementItemDO> webPage);

    default List<SupplierSettlementItemDO> combination(List<WarehouseEntryDO> warehouseEntryList, Map<Long, List<WarehouseEntryProductDO>> warehouseEntryProductMap){
        List<SupplierSettlementItemDO> itemList = new ArrayList<>();
        // 循环入库单
        for (WarehouseEntryDO warehouseEntryDO : warehouseEntryList) {
            List<WarehouseEntryProductDO> entryProductList = warehouseEntryProductMap.get(warehouseEntryDO.getId());
            // 循环入库单的商品
            for (WarehouseEntryProductDO entryProductDO : entryProductList) {
                SupplierSettlementItemDO supplierSettlementItemDO = new SupplierSettlementItemDO();

                supplierSettlementItemDO.setWarehouseEntryId(warehouseEntryDO.getId());
                supplierSettlementItemDO.setWarehouseEntrySn(warehouseEntryDO.getSn());
                supplierSettlementItemDO.setWarehouseEntryItemId(entryProductDO.getId());
                supplierSettlementItemDO.setGoodsId(entryProductDO.getGoodsId());
                supplierSettlementItemDO.setProductId(entryProductDO.getProductId());
                supplierSettlementItemDO.setProductSn(entryProductDO.getSn());
                supplierSettlementItemDO.setProductName(entryProductDO.getName());
                supplierSettlementItemDO.setProductSpecification(entryProductDO.getSpecification());
                supplierSettlementItemDO.setProductUnit(entryProductDO.getUnit());
                supplierSettlementItemDO.setProductBarcode(entryProductDO.getProductBarcode());
                supplierSettlementItemDO.setCategoryId(entryProductDO.getCategoryId());
                supplierSettlementItemDO.setCategoryName(entryProductDO.getCategoryName());
                supplierSettlementItemDO.setProductPrice(entryProductDO.getContractPrice());
                supplierSettlementItemDO.setProductCostPrice(entryProductDO.getCostPrice());
                supplierSettlementItemDO.setTaxRate(entryProductDO.getTaxRate());
                supplierSettlementItemDO.setProductNum(entryProductDO.getNum() - (entryProductDO.getReturnNum()==null?0:entryProductDO.getReturnNum()));
                supplierSettlementItemDO.setTotalPrice(CurrencyUtil.mul(entryProductDO.getContractPrice(), supplierSettlementItemDO.getProductNum()));

                itemList.add(supplierSettlementItemDO);
            }
        }

        return itemList;
    }
}

