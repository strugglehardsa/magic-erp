package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.framework.util.DateUtil;
import com.hys.app.model.erp.dos.ProductDO;
import com.hys.app.model.erp.dos.StockBatchFlowDO;
import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import com.hys.app.model.erp.dto.StockUpdateDTO;
import com.hys.app.model.erp.enums.StockChangeSourceEnum;
import com.hys.app.model.erp.enums.StockOperateEnum;
import com.hys.app.model.erp.vo.WarehouseEntryBatchVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.hys.app.framework.util.CollectionUtils.convertMap;

/**
 * 入库批次 Convert
 *
 * @author 张崧
 * @since 2023-12-08 11:49:52
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface WarehouseEntryBatchConverter {

    List<WarehouseEntryBatchVO> convert(List<WarehouseEntryBatchDO> list);

    WarehouseEntryBatchVO convert(WarehouseEntryBatchDO warehouseEntryBatchDO);

    WebPage<WarehouseEntryBatchVO> convert(WebPage<WarehouseEntryBatchDO> webPage);

    @Mapping(source = "batchDO.id", target = "batchId")
    @Mapping(source = "batchDO.sn", target = "batchSn")
    @Mapping(source = "batchDO.entryPrice", target = "productPrice")
    @Mapping(source = "batchDO.remainNum", target = "batchRemainNum")
    @Mapping(target = "id", ignore = true)
    StockBatchFlowDO convertFlow(WarehouseEntryBatchDO batchDO);

    default List<StockBatchFlowDO> convertFlowList(StockChangeSourceEnum sourceType, String sourceSn, List<WarehouseEntryBatchDO> batchList) {
        long dateline = DateUtil.getDateline();
        return batchList.stream().map(warehouseEntryBatchDO -> {
            StockBatchFlowDO stockBatchFlowDO = convertFlow(warehouseEntryBatchDO);

            stockBatchFlowDO.setChangeType(StockOperateEnum.Increase);
            // 新创建的批次，入库数量就是变更数量
            stockBatchFlowDO.setChangeNum(warehouseEntryBatchDO.getEntryNum());
            stockBatchFlowDO.setSourceType(sourceType);
            stockBatchFlowDO.setSourceSn(sourceSn);
            stockBatchFlowDO.setCreateTime(dateline);

            return stockBatchFlowDO;
        }).collect(Collectors.toList());
    }

    default List<StockBatchFlowDO> convertFlowList(StockChangeSourceEnum sourceType, String sourceSn, Map<Long, WarehouseEntryBatchDO> batchMap, List<StockUpdateDTO> updateStockList) {
        long dateline = DateUtil.getDateline();
        return updateStockList.stream().map(stockUpdateDTO -> {
            WarehouseEntryBatchDO batchDO = batchMap.get(stockUpdateDTO.getBatchId());

            StockBatchFlowDO stockBatchFlowDO = convertFlow(batchDO);

            stockBatchFlowDO.setChangeType(stockUpdateDTO.getOperate());
            stockBatchFlowDO.setChangeNum(stockUpdateDTO.getChangeNum());
            stockBatchFlowDO.setSourceType(sourceType);
            stockBatchFlowDO.setSourceSn(sourceSn);
            stockBatchFlowDO.setCreateTime(dateline);

            return stockBatchFlowDO;
        }).collect(Collectors.toList());
    }

    default WebPage<WarehouseEntryBatchVO> convert(WebPage<WarehouseEntryBatchDO> webPage, List<ProductDO> productList){
        WebPage<WarehouseEntryBatchVO> convert = convert(webPage);
        Map<Long, ProductDO> productMap = convertMap(productList, ProductDO::getId, Function.identity());

        for (WarehouseEntryBatchVO batchVO : convert.getData()) {
            batchVO.setProduct(productMap.get(batchVO.getProductId()));
        }
        return convert;
    }
}

