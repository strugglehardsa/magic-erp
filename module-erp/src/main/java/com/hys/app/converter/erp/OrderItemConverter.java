package com.hys.app.converter.erp;

import com.hys.app.framework.database.WebPage;
import com.hys.app.model.erp.dos.OrderDO;
import com.hys.app.model.erp.dos.OrderItemDO;
import com.hys.app.model.erp.dos.ProductDO;
import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import com.hys.app.model.erp.dto.OrderDTO;
import com.hys.app.model.erp.dto.OrderItemDTO;
import com.hys.app.model.erp.dto.StockUpdateDTO;
import com.hys.app.model.erp.enums.OrderTypeEnum;
import com.hys.app.model.erp.enums.StockOperateEnum;
import com.hys.app.model.erp.vo.OrderItemExcelVO;
import com.hys.app.model.erp.vo.OrderItemVO;
import com.hys.app.model.goods.dos.CategoryDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 订单明细 Converter
 *
 * @author 张崧
 * 2024-01-24 16:39:38
 */
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface OrderItemConverter {

    OrderItemDO convert(OrderItemDTO orderItemDTO);

    OrderItemVO convert(OrderItemDO orderItemDO);

    List<OrderItemVO> convertList(List<OrderItemDO> list);

    List<OrderItemExcelVO> convertExcel(List<OrderItemVO> list);

    WebPage<OrderItemVO> convertPage(WebPage<OrderItemDO> webPage);

    default List<OrderItemDO> convert(OrderDTO orderDTO, OrderDO orderDO) {
        List<OrderItemDTO> itemList = orderDTO.getItemList();
        Map<Long, ProductDO> productMap = orderDTO.getProductMap();
        Map<Long, WarehouseEntryBatchDO> batchMap = orderDTO.getBatchMap();
        Map<Long, CategoryDO> categoryMap = orderDTO.getCategoryMap();

        return itemList.stream().map(orderItemDTO -> {

            OrderItemDO orderItemDO = convert(orderItemDTO);
            // 订单信息
            orderItemDO.setOrderId(orderDO.getId());
            orderItemDO.setOrderSn(orderDO.getSn());

            if (orderDTO.getType() == OrderTypeEnum.TO_C) {
                WarehouseEntryBatchDO batchDO = batchMap.get(orderItemDTO.getBatchId());
                CategoryDO categoryDO = categoryMap.get(batchDO.getCategoryId());

                // 商品信息
                orderItemDO.setBatchId(batchDO.getId());
                orderItemDO.setBatchSn(batchDO.getSn());
                orderItemDO.setGoodsId(batchDO.getGoodsId());
                orderItemDO.setProductId(batchDO.getProductId());
                orderItemDO.setProductSn(batchDO.getProductSn());
                orderItemDO.setProductName(batchDO.getProductName());
                orderItemDO.setProductSpecification(batchDO.getProductSpecification());
                orderItemDO.setProductUnit(batchDO.getProductUnit());
                orderItemDO.setCategoryId(batchDO.getCategoryId());
                orderItemDO.setCategoryName(categoryDO == null ? "" : categoryDO.getName());

            } else {
                ProductDO productDO = productMap.get(orderItemDTO.getProductId());
                CategoryDO categoryDO = categoryMap.get(productDO.getCategoryId());
                // 商品信息
                orderItemDO.setGoodsId(productDO.getGoodsId());
                orderItemDO.setProductId(productDO.getId());
                orderItemDO.setProductSn(productDO.getSn());
                orderItemDO.setProductName(productDO.getName());
                orderItemDO.setProductSpecification(productDO.getSpecification());
                orderItemDO.setProductUnit(productDO.getUnit());
                orderItemDO.setCategoryId(productDO.getCategoryId());
                orderItemDO.setCategoryName(categoryDO == null ? "" : categoryDO.getName());
            }

            orderItemDO.setReturnNum(0);

            return orderItemDO;
        }).collect(Collectors.toList());
    }

    default List<StockUpdateDTO> convertStockUpdate(List<OrderItemDO> itemList){
        return itemList.stream().map(orderItemDO -> {
            StockUpdateDTO stockUpdateDTO = new StockUpdateDTO();

            stockUpdateDTO.setBatchId(orderItemDO.getBatchId());
            stockUpdateDTO.setOperate(StockOperateEnum.Reduce);
            stockUpdateDTO.setTip(orderItemDO.getProductName());
            stockUpdateDTO.setChangeNum(orderItemDO.getNum());

            return stockUpdateDTO;
        }).collect(Collectors.toList());
    }
}

