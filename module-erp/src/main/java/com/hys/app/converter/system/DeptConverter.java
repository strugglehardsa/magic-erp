package com.hys.app.converter.system;

import com.hys.app.model.system.dto.DeptCreateDTO;
import com.hys.app.model.system.vo.DeptRespVO;
import com.hys.app.model.system.vo.DeptSimpleRespVO;
import com.hys.app.model.system.dto.DeptUpdateDTO;
import com.hys.app.model.base.context.Region;
import com.hys.app.model.system.dos.DeptDO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.Arrays;
import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface DeptConverter {

    List<DeptRespVO> convertList(List<DeptDO> list);

    List<DeptSimpleRespVO> convertList02(List<DeptDO> list);

    DeptRespVO convert(DeptDO bean);

    DeptDO convert(DeptCreateDTO bean);

    DeptDO convert(DeptUpdateDTO bean);

    default DeptDO convert(DeptCreateDTO reqVO, Region region){
        DeptDO deptDO = convert(reqVO);

        deptDO.setRegionIds(Arrays.asList(region.getProvinceId(), region.getCityId(), region.getCountyId(), region.getTownId()));
        deptDO.setRegionNames(Arrays.asList(region.getProvince(), region.getCity(), region.getCounty(), region.getCounty()));

        return deptDO;
    }

    default DeptDO convert(DeptUpdateDTO reqVO, Region region){
        DeptDO deptDO = convert(reqVO);

        deptDO.setRegionIds(Arrays.asList(region.getProvinceId(), region.getCityId(), region.getCountyId(), region.getTownId()));
        deptDO.setRegionNames(Arrays.asList(region.getProvince(), region.getCity(), region.getCounty(), region.getCounty()));

        return deptDO;
    }

}
