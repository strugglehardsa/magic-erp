package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.SupplierReturnDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 供应商退货VO
 *
 * @author 张崧
 * @since 2023-12-14 11:12:46
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SupplierReturnVO extends SupplierReturnDO {

    @ApiModelProperty(name = "allowable", value = "允许的操作")
    private SupplierReturnAllowable allowable;

    @ApiModelProperty(name = "item_list", value = "退货项列表")
    private List<SupplierReturnItemVO> itemList;

}

