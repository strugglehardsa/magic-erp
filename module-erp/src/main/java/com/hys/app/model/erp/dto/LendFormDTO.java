package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 商品借出单实体DTO
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class LendFormDTO implements Serializable {

    private static final long serialVersionUID = 9084801405847995621L;

    /**
     * 部门ID
     */
    @ApiModelProperty(name = "dept_id", value = "部门ID", required = true)
    @NotNull(message = "部门ID不能为空")
    private Long deptId;
    /**
     * 部门名称
     */
    @ApiModelProperty(name = "dept_name", value = "部门名称", required = true)
    @NotEmpty(message = "部门名称不能为空")
    private String deptName;
    /**
     * 仓库ID
     */
    @ApiModelProperty(name = "warehouse_id", value = "仓库ID")
    @NotNull(message = "仓库ID不能为空")
    private Long warehouseId;
    /**
     * 仓库名称
     */
    @ApiModelProperty(name = "warehouse_name", value = "仓库名称", required = true)
    @NotEmpty(message = "仓库名称不能为空")
    private String warehouseName;
    /**
     * 借出人ID
     */
    @ApiModelProperty(name = "lend_person_id", value = "借出人ID", required = true)
    @NotNull(message = "借出人ID不能为空")
    private Long lendPersonId;
    /**
     * 借出人
     */
    @ApiModelProperty(name = "lend_person", value = "借出人", required = true)
    @NotEmpty(message = "借出人不能为空")
    private String lendPerson;
    /**
     * 借出登记人ID
     */
    @ApiModelProperty(name = "lend_register_id", value = "借出登记人ID", required = true)
    @NotNull(message = "借出登记人ID不能为空")
    private Long lendRegisterId;
    /**
     * 借出登记人
     */
    @ApiModelProperty(name = "lend_register", value = "借出登记人", required = true)
    @NotEmpty(message = "借出登记人不能为空")
    private String lendRegister;
    /**
     * 借出时间
     */
    @ApiModelProperty(name = "lend_time", value = "借出时间", required = true)
    @NotNull(message = "借出时间不能为空")
    private Long lendTime;
    /**
     * 借出说明
     */
    @ApiModelProperty(name = "lend_desc", value = "借出说明", required = true)
    @NotEmpty(message = "借出说明不能为空")
    private String lendDesc;
    /**
     * 借出单产品集合
     */
    @ApiModelProperty(name = "product_list", value = "借出单产品集合", required = true)
    @NotNull(message = "借出单产品不能为空")
    private List<LendFormProductDTO> productList;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getLendPersonId() {
        return lendPersonId;
    }

    public void setLendPersonId(Long lendPersonId) {
        this.lendPersonId = lendPersonId;
    }

    public String getLendPerson() {
        return lendPerson;
    }

    public void setLendPerson(String lendPerson) {
        this.lendPerson = lendPerson;
    }

    public Long getLendRegisterId() {
        return lendRegisterId;
    }

    public void setLendRegisterId(Long lendRegisterId) {
        this.lendRegisterId = lendRegisterId;
    }

    public String getLendRegister() {
        return lendRegister;
    }

    public void setLendRegister(String lendRegister) {
        this.lendRegister = lendRegister;
    }

    public Long getLendTime() {
        return lendTime;
    }

    public void setLendTime(Long lendTime) {
        this.lendTime = lendTime;
    }

    public String getLendDesc() {
        return lendDesc;
    }

    public void setLendDesc(String lendDesc) {
        this.lendDesc = lendDesc;
    }

    public List<LendFormProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<LendFormProductDTO> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "LendFormDTO{" +
                "deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", warehouseId=" + warehouseId +
                ", warehouseName='" + warehouseName + '\'' +
                ", lendPersonId=" + lendPersonId +
                ", lendPerson='" + lendPerson + '\'' +
                ", lendRegisterId=" + lendRegisterId +
                ", lendRegister='" + lendRegister + '\'' +
                ", lendTime=" + lendTime +
                ", lendDesc='" + lendDesc + '\'' +
                ", productList=" + productList +
                '}';
    }
}
