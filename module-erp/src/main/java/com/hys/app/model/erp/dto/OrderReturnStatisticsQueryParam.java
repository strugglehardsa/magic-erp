package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 订单退货统计查询参数实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class OrderReturnStatisticsQueryParam extends OrderReturnQueryParams implements Serializable {

    private static final long serialVersionUID = 1686876732623125173L;

    @ApiModelProperty(name = "category_id", value = "分类ID")
    private Long categoryId;

    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public String toString() {
        return "OrderReturnStatisticsQueryParam{" +
                "categoryId=" + categoryId +
                ", productName='" + productName + '\'' +
                '}';
    }
}
