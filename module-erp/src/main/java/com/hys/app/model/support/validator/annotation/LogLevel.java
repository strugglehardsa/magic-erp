package com.hys.app.model.support.validator.annotation;
/**
 * 日志级别
 * @author kingapex
 * @data 2021/11/24 14:21
 * @version 1.0
 **/
public enum LogLevel {
    /**
     * 一般的、日常级别
     */
    normal,
    /**
     * 重要的级别
     */
    important
}
