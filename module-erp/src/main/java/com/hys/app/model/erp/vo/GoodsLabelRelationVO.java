package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.GoodsDO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 商品标签-商品关联表详情
 *
 * @author 张崧
 * 2024-03-20 16:29:17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "商品标签-商品关联表详情")
public class GoodsLabelRelationVO extends GoodsDO {

    @ApiModelProperty(name = "label_name", value = "标签名称")
    private String labelName;

}

