package com.hys.app.model.erp.vo;

import lombok.Data;
import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 企业excel导出
 *
 * @author 张崧
 * 2024-03-25 15:44:10
 */
@Data
public class EnterpriseExcelVO {

    @ExcelProperty("企业名称")
    private String name;
    
    @ExcelProperty("编号")
    private String sn;
    
    @ExcelProperty("手机号")
    private String mobile;
    
    @ExcelProperty("邮箱")
    private String email;
    
    @ExcelProperty("邮编")
    private String zipCode;
    
    @ExcelProperty("纳税人识别号")
    private String taxNum;
    
    @ExcelProperty("开户银行")
    private String bankName;
    
    @ExcelProperty("地址")
    private String address;
    
    @ExcelProperty("联系人")
    private String linkman;
    
    @ExcelProperty("联系电话")
    private String linkPhone;
    
    @ExcelProperty("传真")
    private String fax;
    
    @ExcelProperty("银行账号")
    private String bankAccount;
    
    @ExcelProperty("银行户名")
    private String bankAccountName;
    
    @ExcelProperty("地区id集合")
    private String regionIds;
    
    @ExcelProperty("地区名称集合")
    private String regionNames;
    
    @ExcelProperty("备注")
    private String remark;
    
}

