package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 商品标签分页查询参数
 *
 * @author 张崧
 * 2024-03-20 16:23:19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "商品标签分页查询参数")
public class GoodsLabelQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "name", value = "标签名称")
    private String name;
    
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long[] createTime;
    
}

