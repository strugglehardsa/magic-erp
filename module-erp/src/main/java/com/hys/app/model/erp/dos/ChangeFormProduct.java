package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 换货单产品实体
 * @Author dmy
 * 2023-12-05
 */
@TableName("es_change_form_product")
@ApiModel
public class ChangeFormProduct implements Serializable {

    private static final long serialVersionUID = -4237870341806191017L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 换货单ID
     */
    @ApiModelProperty(name = "change_id", value = "换货单ID")
    private Long changeId;
    /**
     * 入库单ID
     */
    @ApiModelProperty(name = "stock_id", value = "入库单ID")
    private Long stockId;
    /**
     * 入库单编号
     */
    @ApiModelProperty(name = "stock_sn", value = "入库单编号")
    private String stockSn;
    /**
     * 入库单明细id
     */
    @ApiModelProperty(name = "stock_item_id", value = "入库单明细id")
    private Long stockItemId;
    /**
     * 入库批次ID
     */
    @ApiModelProperty(name = "stock_batch_id", value = "入库批次ID")
    private Long stockBatchId;
    /**
     * 入库批次编号
     */
    @ApiModelProperty(name = "stock_batch_sn", value = "入库批次编号")
    private String stockBatchSn;
    /**
     * 商品ID
     */
    @ApiModelProperty(name = "goods_id", value = "商品ID")
    private Long goodsId;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID")
    private Long productId;
    /**
     * 产品名称
     */
    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;
    /**
     * 产品编号
     */
    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;
    /**
     * 产品规格
     */
    @ApiModelProperty(name = "specification", value = "产品规格")
    private String specification;
    /**
     * 分类ID
     */
    @ApiModelProperty(name = "category_id", value = "分类ID")
    private Long categoryId;
    /**
     * 分类名称
     */
    @ApiModelProperty(name = "category_name", value = "分类名称")
    private String categoryName;
    /**
     * 产品单位
     */
    @ApiModelProperty(name = "unit", value = "产品单位")
    private String unit;
    /**
     * 数量
     */
    @ApiModelProperty(name = "num", value = "数量")
    private Integer num;
    /**
     * 金额
     */
    @ApiModelProperty(name = "amount", value = "金额")
    private Double amount;
    /**
     * 成本价
     */
    @ApiModelProperty(name = "cost_price", value = "成本价")
    private Double costPrice;
    /**
     * 产品类型 0：退货，1：换货
     */
    @ApiModelProperty(name = "type", value = "产品类型 0：退货，1：换货")
    private Integer type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChangeId() {
        return changeId;
    }

    public void setChangeId(Long changeId) {
        this.changeId = changeId;
    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public String getStockSn() {
        return stockSn;
    }

    public void setStockSn(String stockSn) {
        this.stockSn = stockSn;
    }

    public Long getStockItemId() {
        return stockItemId;
    }

    public void setStockItemId(Long stockItemId) {
        this.stockItemId = stockItemId;
    }

    public Long getStockBatchId() {
        return stockBatchId;
    }

    public void setStockBatchId(Long stockBatchId) {
        this.stockBatchId = stockBatchId;
    }

    public String getStockBatchSn() {
        return stockBatchSn;
    }

    public void setStockBatchSn(String stockBatchSn) {
        this.stockBatchSn = stockBatchSn;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ChangeFormProduct that = (ChangeFormProduct) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(changeId, that.changeId) &&
                Objects.equals(stockId, that.stockId) &&
                Objects.equals(stockSn, that.stockSn) &&
                Objects.equals(stockBatchId, that.stockBatchId) &&
                Objects.equals(stockBatchSn, that.stockBatchSn) &&
                Objects.equals(goodsId, that.goodsId) &&
                Objects.equals(productId, that.productId) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(productSn, that.productSn) &&
                Objects.equals(specification, that.specification) &&
                Objects.equals(categoryId, that.categoryId) &&
                Objects.equals(categoryName, that.categoryName) &&
                Objects.equals(unit, that.unit) &&
                Objects.equals(num, that.num) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(costPrice, that.costPrice) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, changeId, stockId, stockSn, stockBatchId, stockBatchSn, goodsId, productId, productName, productSn, specification, categoryId, categoryName, unit, num, amount, costPrice, type);
    }

    @Override
    public String toString() {
        return "ChangeFormProduct{" +
                "id=" + id +
                ", changeId=" + changeId +
                ", stockId='" + stockId + '\'' +
                ", stockSn='" + stockSn + '\'' +
                ", stockBatchId=" + stockBatchId +
                ", stockBatchSn='" + stockBatchSn + '\'' +
                ", goodsId=" + goodsId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", specification='" + specification + '\'' +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", unit='" + unit + '\'' +
                ", num=" + num +
                ", amount=" + amount +
                ", costPrice=" + costPrice +
                ", type=" + type +
                '}';
    }
}
