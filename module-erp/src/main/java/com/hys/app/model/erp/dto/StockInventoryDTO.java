package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 库存盘点单实体DTO
 *
 * @author dmy
 * 2023-12-05
 */
public class StockInventoryDTO implements Serializable {

    private static final long serialVersionUID = -158888827316508502L;

    /**
     * 部门ID
     */
    @ApiModelProperty(name = "dept_id", value = "部门ID", required = true)
    @NotNull(message = "部门ID不能为空")
    private Long deptId;
    /**
     * 部门名称
     */
    @ApiModelProperty(name = "dept_name", value = "部门名称", required = true)
    @NotEmpty(message = "部门名称不能为空")
    private String deptName;
    /**
     * 仓库ID
     */
    @ApiModelProperty(name = "warehouse_id", value = "仓库ID", required = true)
    @NotNull(message = "仓库ID不能为空")
    private Long warehouseId;
    /**
     * 仓库名称
     */
    @ApiModelProperty(name = "warehouse_name", value = "仓库名称", required = true)
    @NotEmpty(message = "仓库名称不能为空")
    private String warehouseName;
    /**
     * 盘点人ID
     */
    @ApiModelProperty(name = "inventory_person_id", value = "盘点人ID", required = true)
    @NotNull(message = "盘点人ID不能为空")
    private Long inventoryPersonId;
    /**
     * 盘点人
     */
    @ApiModelProperty(name = "inventory_person", value = "盘点人", required = true)
    @NotEmpty(message = "盘点人不能为空")
    private String inventoryPerson;
    /**
     * 制单人ID
     */
    @ApiModelProperty(name = "creator_id", value = "制单人ID", required = true)
    @NotNull(message = "制单人ID不能为空")
    private Long creatorId;
    /**
     * 制单人
     */
    @ApiModelProperty(name = "creator", value = "制单人", required = true)
    @NotEmpty(message = "制单人不能为空")
    private String creator;
    /**
     * 盘点时间
     */
    @ApiModelProperty(name = "inventory_time", value = "盘点时间", required = true)
    @NotNull(message = "盘点时间不能为空")
    private Long inventoryTime;
    /**
     * 盘点说明
     */
    @ApiModelProperty(name = "inventory_desc", value = "盘点说明")
    private String inventoryDesc;
    /**
     * 库存盘点单产品集合
     */
    @ApiModelProperty(name = "product_list", value = "库存盘点单产品集合", required = true)
    @NotNull(message = "库存盘点单产品不能为空")
    private List<StockInventoryProductDTO> productList;

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getInventoryPersonId() {
        return inventoryPersonId;
    }

    public void setInventoryPersonId(Long inventoryPersonId) {
        this.inventoryPersonId = inventoryPersonId;
    }

    public String getInventoryPerson() {
        return inventoryPerson;
    }

    public void setInventoryPerson(String inventoryPerson) {
        this.inventoryPerson = inventoryPerson;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getInventoryTime() {
        return inventoryTime;
    }

    public void setInventoryTime(Long inventoryTime) {
        this.inventoryTime = inventoryTime;
    }

    public String getInventoryDesc() {
        return inventoryDesc;
    }

    public void setInventoryDesc(String inventoryDesc) {
        this.inventoryDesc = inventoryDesc;
    }

    public List<StockInventoryProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<StockInventoryProductDTO> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return "StockInventoryDTO{" +
                "deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", warehouseId=" + warehouseId +
                ", warehouseName='" + warehouseName + '\'' +
                ", inventoryPersonId=" + inventoryPersonId +
                ", inventoryPerson='" + inventoryPerson + '\'' +
                ", creatorId=" + creatorId +
                ", creator='" + creator + '\'' +
                ", inventoryTime=" + inventoryTime +
                ", inventoryDesc='" + inventoryDesc + '\'' +
                ", productList=" + productList +
                '}';
    }
}
