package com.hys.app.model.base.message;

import java.io.Serializable;

/**
 * 发送短信结果回调
 * @author zhangsong
 * @version v1.0
 * @since v7.3.0
 * 2021-02-05
 */
public class SmsSendCallbackMsg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 652862137182690060L;

	/**
	 * 短信回执id
	 */
	private String bizId;
	/**
	 * 用户接收是否成功
	 */
	private String isSuccess;

	public String getBizId() {
		return bizId;
	}

	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

	public String getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(String isSuccess) {
		this.isSuccess = isSuccess;
	}
}
