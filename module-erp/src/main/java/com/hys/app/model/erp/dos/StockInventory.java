package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.model.erp.vo.StockInventoryAllowable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 库存盘点单实体
 *
 * @author dmy
 * 2023-12-05
 */
@TableName("es_stock_inventory")
@ApiModel
public class StockInventory implements Serializable {

    private static final long serialVersionUID = -6325801216669289202L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 编号
     */
    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    /**
     * 部门ID
     */
    @ApiModelProperty(name = "dept_id", value = "部门ID")
    private Long deptId;
    /**
     * 部门名称
     */
    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;
    /**
     * 仓库ID
     */
    @ApiModelProperty(name = "warehouse_id", value = "仓库ID")
    private Long warehouseId;
    /**
     * 仓库名称
     */
    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;
    /**
     * 盘点人ID
     */
    @ApiModelProperty(name = "inventory_person_id", value = "盘点人ID")
    private Long inventoryPersonId;
    /**
     * 盘点人
     */
    @ApiModelProperty(name = "inventory_person", value = "盘点人")
    private String inventoryPerson;
    /**
     * 制单人ID
     */
    @ApiModelProperty(name = "creator_id", value = "制单人ID")
    private Long creatorId;
    /**
     * 制单人
     */
    @ApiModelProperty(name = "creator", value = "制单人")
    private String creator;
    /**
     * 盘点时间
     */
    @ApiModelProperty(name = "inventory_time", value = "盘点时间")
    private Long inventoryTime;
    /**
     * 盘点说明
     */
    @ApiModelProperty(name = "inventory_desc", value = "盘点说明")
    private String inventoryDesc;
    /**
     * 创建时间
     */
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long createTime;
    /**
     * 状态
     * @see com.hys.app.model.erp.enums.StockInventoryStatusEnum
     */
    @ApiModelProperty(name = "status", value = "状态")
    private String status;
    /**
     * 审核驳回原因
     */
    @ApiModelProperty(name = "reject_reason", value = "审核驳回原因")
    private String rejectReason;
    /**
     * 审核时间
     */
    @ApiModelProperty(name = "audit_time", value = "审核时间")
    private Long auditTime;
    /**
     * 审核人ID
     */
    @ApiModelProperty(name = "audit_person_id", value = "审核人ID")
    private Long auditPersonId;
    /**
     * 审核人名称
     */
    @ApiModelProperty(name = "audit_person_name", value = "审核人名称")
    private String auditPersonName;
    /**
     * 库存盘点单允许进行的操作
     */
    @TableField(exist = false)
    @ApiModelProperty(name = "allowable", value = "库存盘点单允许进行的操作")
    private StockInventoryAllowable allowable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Long getInventoryPersonId() {
        return inventoryPersonId;
    }

    public void setInventoryPersonId(Long inventoryPersonId) {
        this.inventoryPersonId = inventoryPersonId;
    }

    public String getInventoryPerson() {
        return inventoryPerson;
    }

    public void setInventoryPerson(String inventoryPerson) {
        this.inventoryPerson = inventoryPerson;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getInventoryTime() {
        return inventoryTime;
    }

    public void setInventoryTime(Long inventoryTime) {
        this.inventoryTime = inventoryTime;
    }

    public String getInventoryDesc() {
        return inventoryDesc;
    }

    public void setInventoryDesc(String inventoryDesc) {
        this.inventoryDesc = inventoryDesc;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    public Long getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Long auditTime) {
        this.auditTime = auditTime;
    }

    public Long getAuditPersonId() {
        return auditPersonId;
    }

    public void setAuditPersonId(Long auditPersonId) {
        this.auditPersonId = auditPersonId;
    }

    public String getAuditPersonName() {
        return auditPersonName;
    }

    public void setAuditPersonName(String auditPersonName) {
        this.auditPersonName = auditPersonName;
    }

    public StockInventoryAllowable getAllowable() {
        return allowable;
    }

    public void setAllowable(StockInventoryAllowable allowable) {
        this.allowable = allowable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        StockInventory that = (StockInventory) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(sn, that.sn) &&
                Objects.equals(deptId, that.deptId) &&
                Objects.equals(deptName, that.deptName) &&
                Objects.equals(warehouseId, that.warehouseId) &&
                Objects.equals(warehouseName, that.warehouseName) &&
                Objects.equals(inventoryPersonId, that.inventoryPersonId) &&
                Objects.equals(inventoryPerson, that.inventoryPerson) &&
                Objects.equals(creatorId, that.creatorId) &&
                Objects.equals(creator, that.creator) &&
                Objects.equals(inventoryTime, that.inventoryTime) &&
                Objects.equals(inventoryDesc, that.inventoryDesc) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(status, that.status) &&
                Objects.equals(rejectReason, that.rejectReason) &&
                Objects.equals(auditTime, that.auditTime) &&
                Objects.equals(auditPersonId, that.auditPersonId) &&
                Objects.equals(auditPersonName, that.auditPersonName) &&
                Objects.equals(allowable, that.allowable);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sn, deptId, deptName, warehouseId, warehouseName, inventoryPersonId, inventoryPerson, creatorId, creator, inventoryTime, inventoryDesc, createTime, status, rejectReason, auditTime, auditPersonId, auditPersonName, allowable);
    }

    @Override
    public String toString() {
        return "StockInventory{" +
                "id=" + id +
                ", sn='" + sn + '\'' +
                ", deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                ", warehouseId=" + warehouseId +
                ", warehouseName='" + warehouseName + '\'' +
                ", inventoryPersonId=" + inventoryPersonId +
                ", inventoryPerson='" + inventoryPerson + '\'' +
                ", creatorId=" + creatorId +
                ", creator='" + creator + '\'' +
                ", inventoryTime=" + inventoryTime +
                ", inventoryDesc='" + inventoryDesc + '\'' +
                ", createTime=" + createTime +
                ", status='" + status + '\'' +
                ", rejectReason='" + rejectReason + '\'' +
                ", auditTime=" + auditTime +
                ", auditPersonId=" + auditPersonId +
                ", auditPersonName='" + auditPersonName + '\'' +
                ", allowable=" + allowable +
                '}';
    }
}
