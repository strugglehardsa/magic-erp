package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 商品借出单状态枚举
 *
 * @author 张崧
 * @since 2023-12-05
 */
@Getter
@AllArgsConstructor
public enum LendFormStatusEnum {

    /**
     * 新创建
     */
    NEW,
    /**
     * 已确认
     */
    CONFIRMED,
    /**
     * 已退还
     */
    RETURNED
}
