package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 供应商结算单明细查询参数
 *
 * @author 张崧
 * @since 2023-12-15 14:09:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SupplierSettlementItemQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "supplier_settlement_id", value = "供应商结算单id")
    private Long supplierSettlementId;
    
    @ApiModelProperty(name = "warehouse_entry_id", value = "入库单id")
    private Long warehouseEntryId;
    
    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单编号")
    private String warehouseEntrySn;
    
    @ApiModelProperty(name = "product_id", value = "商品id")
    private Long productId;
    
    @ApiModelProperty(name = "product_name", value = "商品名称")
    private String productName;
    
    @ApiModelProperty(name = "product_specification", value = "商品规格")
    private String productSpecification;
    
    @ApiModelProperty(name = "product_unit", value = "商品单位")
    private String productUnit;
    
    @ApiModelProperty(name = "product_price", value = "商品单价")
    private Double productPrice;
    
    @ApiModelProperty(name = "product_num", value = "商品数量")
    private Integer productNum;
    
    @ApiModelProperty(name = "total_price", value = "总价格")
    private Double totalPrice;
    
}

