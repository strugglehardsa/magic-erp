package com.hys.app.model.erp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 出库单发货DTO
 *
 * @author 张崧
 * @since 2023-12-11
 */
@Data
public class WarehouseOutShipDTO {

    @ApiModelProperty(name = "id", value = "出库单id")
    @NotNull(message = "出库单id不能为空")
    private Long id;

    @ApiModelProperty(name = "logistics_company_id", value = "物流公司id")
    private Long logisticsCompanyId;

    @ApiModelProperty(name = "tracking_number", value = "物流单号")
    private String trackingNumber;

    @ApiModelProperty(name = "freight_price", value = "物流费用")
    @NotNull(message = "物流费用不能为空")
    @Min(value = 0, message = "物流费用最小为0")
    private Double freightPrice;

    @ApiModelProperty(name = "consignee", value = "提货人")
    @NotBlank(message = "提货人不能为空")
    private String consignee;


    @JsonIgnore
    @ApiModelProperty(value = "物流公司名称", hidden = true)
    private String logisticsCompanyName;
}

