package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 订单分页查询参数
 *
 * @author 张崧
 * 2024-01-24 15:58:31
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "订单分页查询参数")
public class OrderQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "keywords", value = "关键词")
    private String keywords;

    @ApiModelProperty(name = "sn", value = "订单编号")
    private String sn;

    @ApiModelProperty(name = "status", value = "订单状态")
    private String status;

    @ApiModelProperty(name = "payment_status", value = "支付状态")
    private String paymentStatus;

    @ApiModelProperty(name = "member_id", value = "会员id")
    private Long memberId;
    
    @ApiModelProperty(name = "member_name", value = "会员名称")
    private String memberName;
    
    @ApiModelProperty(name = "order_time", value = "下单时间")
    private Long[] orderTime;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;
    
    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;
    
    @ApiModelProperty(name = "marketing_id", value = "销售经理id")
    private Long marketingId;
    
    @ApiModelProperty(name = "marketing_name", value = "销售经理名称")
    private String marketingName;
    
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;
    
    @ApiModelProperty(name = "delivery_type", value = "配送方式")
    private String deliveryType;
    
    @ApiModelProperty(name = "store_id", value = "自提门店id")
    private Long storeId;
    
    @ApiModelProperty(name = "store_name", value = "自提门店名称")
    private String storeName;
    
    @ApiModelProperty(name = "warehouse_out_flag", value = "是否已出库")
    private Boolean warehouseOutFlag;
    
    @ApiModelProperty(name = "warehouse_out_id", value = "出库单id")
    private Long warehouseOutId;
    
    @ApiModelProperty(name = "ship_flag", value = "是否已发货")
    private Boolean shipFlag;
    
    @ApiModelProperty(name = "ship_time", value = "发货时间")
    private Long[] shipTime;
    
    @ApiModelProperty(name = "logistics_company_id", value = "物流公司id")
    private Long logisticsCompanyId;
    
    @ApiModelProperty(name = "logistics_company_name", value = "物流公司名称")
    private String logisticsCompanyName;
    
    @ApiModelProperty(name = "logistics_tracking_number", value = "物流单号")
    private String logisticsTrackingNumber;

    @ApiModelProperty(name = "type", value = "订单类型")
    private String type;

}

