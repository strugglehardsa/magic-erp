package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.WarehouseOutDO;
import com.hys.app.model.erp.dos.WarehouseOutItemDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 出库单详情
 *
 * @author 张崧
 * @since 2023-12-07 16:50:20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseOutVO extends WarehouseOutDO {

    @ApiModelProperty(name = "allowable", value = "允许的操作")
    private WarehouseOutAllowable allowable;

    @ApiModelProperty(name = "item_list", value = "出库明细列表")
    private List<WarehouseOutItemDO> itemList;

    @ApiModelProperty(name = "order_list", value = "订单列表")
    private List<Order> orderList;


    @Data
    public static class Order {

        @ApiModelProperty(name = "order_id", value = "订单id")
        private Long orderId;

        @ApiModelProperty(name = "order_sn", value = "订单编号")
        private String orderSn;

        @ApiModelProperty(name = "order_item_list", value = "订单项列表")
        private List<OrderItem> orderItemList;

    }

    @Data
    public static class OrderItem {

        @ApiModelProperty(name = "order_item_id", value = "订单项id")
        private Long orderItemId;

        @ApiModelProperty(name = "product_id", value = "产品id")
        private Long productId;

        @ApiModelProperty(name = "product_sn", value = "产品编号")
        private String productSn;

        @ApiModelProperty(name = "product_name", value = "产品名称")
        private String productName;

        @ApiModelProperty(name = "product_specification", value = "产品规格")
        private String productSpecification;

        @ApiModelProperty(name = "product_unit", value = "产品单位")
        private String productUnit;

        @ApiModelProperty(name = "num", value = "销售数量")
        private Integer num;

        @ApiModelProperty(name = "batch_list", value = "批次列表")
        private List<StockBatch> batchList;

    }

    @Data
    public static class StockBatch {
        @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单编号")
        private String warehouseEntrySn;

        @ApiModelProperty(name = "id", value = "入库批次id")
        private Long batchId;

        @ApiModelProperty(name = "sn", value = "入库批次号")
        private String batchSn;

        @ApiModelProperty(name = "remain_num", value = "剩余库存数量")
        private Integer remainNum;

        @ApiModelProperty(name = "out_num", value = "出库数量")
        private Integer outNum;

    }

}

