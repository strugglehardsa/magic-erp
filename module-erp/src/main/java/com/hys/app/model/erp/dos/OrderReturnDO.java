package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import com.hys.app.model.erp.enums.OrderReturnStatusEnum;
import com.hys.app.model.erp.enums.OrderTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


/**
 * 订单退货实体类
 *
 * @author 张崧
 * @since 2023-12-14 15:42:07
 */
@TableName("erp_order_return")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OrderReturnDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "sn", value = "退货单编号")
    private String sn;

    @ApiModelProperty(name = "order_id", value = "要退货的订单id")
    private Long orderId;

    @ApiModelProperty(name = "order_sn", value = "要退货的订单编号")
    private String orderSn;

    @ApiModelProperty(name = "order_type", value = "要退货的订单类型")
    private OrderTypeEnum orderType;

    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;

    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    private Long warehouseId;

    @ApiModelProperty(name = "dept_name", value = "部门名称")
    private String deptName;

    @ApiModelProperty(name = "warehouse_name", value = "仓库名称")
    private String warehouseName;

    @ApiModelProperty(name = "distribution_name", value = "销售经理")
    private String distributionName;

    @ApiModelProperty(name = "handle_by_id", value = "经手人id")
    private Long handleById;

    @ApiModelProperty(name = "handle_by_name", value = "经手人名称")
    private String handleByName;

    @ApiModelProperty(name = "status", value = "状态")
    private OrderReturnStatusEnum status;

    @ApiModelProperty(name = "return_time", value = "退货时间")
    private Long returnTime;

    @ApiModelProperty(name = "audit_by_id", value = "审核人id")
    private Long auditById;

    @ApiModelProperty(name = "audit_by", value = "审核人")
    private String auditBy;

    @ApiModelProperty(name = "audit_remark", value = "审核备注")
    private String auditRemark;

    @ApiModelProperty(name = "return_remark", value = "退货说明")
    private String returnRemark;

}
