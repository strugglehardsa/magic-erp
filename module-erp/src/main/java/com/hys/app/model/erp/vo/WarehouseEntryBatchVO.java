package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.ProductDO;
import com.hys.app.model.erp.dos.WarehouseEntryBatchDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 入库批次详情
 *
 * @author 张崧
 * @since 2023-12-08 11:49:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseEntryBatchVO extends WarehouseEntryBatchDO {

    @ApiModelProperty(name = "product", value = "产品")
    private ProductDO product;
}

