package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import com.hys.app.model.erp.dto.NoGenerateRuleItem;
import com.hys.app.model.erp.enums.NoBusinessTypeEnum;
import com.hys.app.model.erp.enums.NoSeqResetTypeEnum;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.ArrayList;
import java.util.Date;


/**
 * 编号生成规则实体类
 *
 * @author 张崧
 * 2023-12-01 11:37:56
 */
@TableName(value = "erp_no_generate_rule", autoResultMap = true)
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class NoGenerateRuleDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "type", value = "业务类型")
    private NoBusinessTypeEnum type;

    @ApiModelProperty(name = "name", value = "规则名称")
    private String name;

    @ApiModelProperty(name = "seq_generate_type", value = "顺序号的重置规则")
    private NoSeqResetTypeEnum seqGenerateType;

    @ApiModelProperty(name = "seq_begin_number", value = "顺序号的起始值")
    private Long seqBeginNumber;

    @ApiModelProperty(name = "open_flag", value = "是否启用该规则")
    private Boolean openFlag;

    @ApiModelProperty(name = "item_list", value = "规则项列表")
    @TableField(typeHandler = JacksonTypeHandler.class)
    private RuleItemList itemList;

    @ApiModelProperty(name = "curr_seq_number", value = "当前顺序号")
    private Long currSeqNumber;

    @ApiModelProperty(name = "seq_last_generate_time", value = "最后一次生成顺序号的时间")
    private Date seqLastGenerateTime;

    @Version
    @ApiModelProperty(value = "乐观锁版本号", hidden = true)
    private Long lockVersion;

    public static class RuleItemList extends ArrayList<NoGenerateRuleItem>{

    }

}
