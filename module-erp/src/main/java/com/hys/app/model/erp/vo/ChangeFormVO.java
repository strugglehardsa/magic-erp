package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.ChangeForm;
import com.hys.app.model.erp.dos.ChangeFormProduct;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 换货单实体VO
 *
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class ChangeFormVO extends ChangeForm implements Serializable {

    private static final long serialVersionUID = -4619853435639144887L;

    /**
     * 退货商品信息集合
     */
    @ApiModelProperty(name = "return_list", value = "退货商品信息集合")
    private List<ChangeFormProduct> returnList;
    /**
     * 换货商品信息集合
     */
    @ApiModelProperty(name = "change_list", value = "换货商品信息集合")
    private List<ChangeFormProduct> changeList;
    /**
     * 换货商品金额是否一样
     */
    @ApiModelProperty(name = "check_amount_same", value = "换货商品金额是否一样")
    private Boolean checkAmountSame;
    /**
     * 换货商品金额差额
     */
    @ApiModelProperty(name = "diff_amount", value = "换货商品金额差额")
    private Double diffAmount;
    /**
     * 金额不一致提示信息
     */
    @ApiModelProperty(name = "diff_amount_message", value = "金额不一致提示信息")
    private String diffAmountMessage;

    public List<ChangeFormProduct> getReturnList() {
        return returnList;
    }

    public void setReturnList(List<ChangeFormProduct> returnList) {
        this.returnList = returnList;
    }

    public List<ChangeFormProduct> getChangeList() {
        return changeList;
    }

    public void setChangeList(List<ChangeFormProduct> changeList) {
        this.changeList = changeList;
    }

    public Boolean getCheckAmountSame() {
        return checkAmountSame;
    }

    public void setCheckAmountSame(Boolean checkAmountSame) {
        this.checkAmountSame = checkAmountSame;
    }

    public Double getDiffAmount() {
        return diffAmount;
    }

    public void setDiffAmount(Double diffAmount) {
        this.diffAmount = diffAmount;
    }

    public String getDiffAmountMessage() {
        return diffAmountMessage;
    }

    public void setDiffAmountMessage(String diffAmountMessage) {
        this.diffAmountMessage = diffAmountMessage;
    }

    @Override
    public String toString() {
        return "ChangeFormVO{" +
                "returnList=" + returnList +
                ", changeList=" + changeList +
                ", checkAmountSame=" + checkAmountSame +
                ", diffAmount=" + diffAmount +
                ", diffAmountMessage='" + diffAmountMessage + '\'' +
                '}';
    }
}
