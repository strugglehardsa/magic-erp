package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 订单支付明细分页查询参数
 *
 * @author 张崧
 * 2024-01-24 17:00:32
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "订单支付明细分页查询参数")
public class OrderPaymentQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "order_id", value = "订单id")
    private Long orderId;
    
    @ApiModelProperty(name = "collecting_account_id", value = "收款账户id")
    private Long collectingAccountId;
    
    @ApiModelProperty(name = "collecting_account_name", value = "收款账户名称")
    private String collectingAccountName;
    
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;
    
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long[] createTime;
    
}

