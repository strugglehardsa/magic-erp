package com.hys.app.model.system.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 任务 新增|编辑 请求参数
 *
 * @author 张崧
 * 2024-01-23 10:43:35
 */
@Data
@ApiModel(value = "任务 新增|编辑 请求参数")
public class TaskDTO {

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "name", value = "名称")
    @NotBlank(message = "名称不能为空")
    private String name;
    
    @ApiModelProperty(name = "type", value = "类型")
    @NotBlank(message = "类型不能为空")
    private String type;
    
    @ApiModelProperty(name = "sub_type", value = "子类型")
    @NotBlank(message = "子类型不能为空")
    private String subType;
    
    @ApiModelProperty(name = "params", value = "执行参数")
    @NotBlank(message = "执行参数不能为空")
    private String params;
    
    @ApiModelProperty(name = "status", value = "状态")
    @NotBlank(message = "状态不能为空")
    private String status;
    
    @ApiModelProperty(name = "result", value = "执行结果")
    @NotBlank(message = "执行结果不能为空")
    private String result;
    
    @ApiModelProperty(name = "thread_id", value = "线程id")
    @NotBlank(message = "线程id不能为空")
    private String threadId;
    
}

