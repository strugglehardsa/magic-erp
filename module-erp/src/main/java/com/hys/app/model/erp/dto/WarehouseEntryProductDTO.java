package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 入库单商品明细新增/编辑DTO
 *
 * @author 张崧
 * @since 2023-12-05 15:30:10
 */
@Data
public class WarehouseEntryProductDTO {

    @ApiModelProperty(name = "product_id", value = "商品id")
    @NotNull(message = "商品id不能为空")
    private Long productId;

    @ApiModelProperty(name = "num", value = "本次入库数量")
    @NotNull(message = "本次入库数量不能为空")
    @Min(value = 1, message = "入库数量最小为1")
    private Integer num;

    @ApiModelProperty(name = "price", value = "单价")
    @NotNull(message = "本次入库单价不能为空")
    private Double price;

    @ApiModelProperty(name = "tax_rate", value = "税率")
    private Double taxRate;
}

