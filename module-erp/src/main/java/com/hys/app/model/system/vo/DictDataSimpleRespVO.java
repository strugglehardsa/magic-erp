package com.hys.app.model.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "管理后台 - 数据字典精简 Response VO")
@Data
public class DictDataSimpleRespVO {

    @ApiModelProperty(value = "字典类型", example = "gender")
    private String dictType;

    @ApiModelProperty(value = "字典键值", example = "1")
    private String value;

    @ApiModelProperty(value = "字典标签", example = "男")
    private String label;

    @ApiModelProperty(value = "是否为默认值", example = "true")
    private Boolean defaultFlag;

}
