package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 出库单商品明细查询参数
 *
 * @author 张崧
 * @since 2023-12-07 17:06:32
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class WarehouseOutItemQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "warehouse_out_id", value = "出库单id")
    private Long warehouseOutId;
    
    @ApiModelProperty(name = "order_sn", value = "订单编号")
    private String orderSn;
    
    @ApiModelProperty(name = "warehouse_entry_sn", value = "入库单编号")
    private String warehouseEntrySn;
    
    @ApiModelProperty(name = "product_id", value = "商品id")
    private Long productId;
    
    @ApiModelProperty(name = "product_sn", value = "商品编号")
    private String productSn;
    
    @ApiModelProperty(name = "product_name", value = "商品名称")
    private String productName;
    
    @ApiModelProperty(name = "product_specification", value = "商品规格")
    private String productSpecification;
    
    @ApiModelProperty(name = "product_unit", value = "商品单位")
    private String productUnit;
    
    @ApiModelProperty(name = "product_price", value = "商品单价")
    private Double productPrice;
    
    @ApiModelProperty(name = "out_num", value = "出库数量")
    private Integer outNum;
    
}

