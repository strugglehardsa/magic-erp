package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 供应商查询参数
 *
 * @author 张崧
 * 2023-11-29 14:20:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SupplierQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "keyword", value = "关键字")
    private String keyword;

    @ApiModelProperty(name = "custom_name", value = "客户名称")
    private String customName;

    @ApiModelProperty(name = "custom_sn", value = "客户编号")
    private String customSn;
    
    @ApiModelProperty(name = "transfer", value = "传值")
    private String transfer;
    
    @ApiModelProperty(name = "zipcode", value = "邮政编码")
    private String zipcode;
    
    @ApiModelProperty(name = "postal_address", value = "通讯地址")
    private String postalAddress;
    
    @ApiModelProperty(name = "telephone", value = "联系电话")
    private String telephone;
    
    @ApiModelProperty(name = "company_website", value = "公司网站")
    private String companyWebsite;
    
    @ApiModelProperty(name = "linkman", value = "联系人")
    private String linkman;
    
    @ApiModelProperty(name = "email", value = "电子邮件")
    private String email;
    
    @ApiModelProperty(name = "mobile", value = "手机号码")
    private String mobile;
    
    @ApiModelProperty(name = "production_address", value = "生产地址")
    private String productionAddress;
    
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

    @ApiModelProperty(name = "dept_id", value = "部门id")
    private Long deptId;
    
}

