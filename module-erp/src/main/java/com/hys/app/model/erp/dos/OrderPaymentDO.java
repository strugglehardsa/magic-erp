package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 订单支付明细 DO
 *
 * @author 张崧
 * 2024-01-24 17:00:32
 */
@TableName("erp_order_payment")
@Data
public class OrderPaymentDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(name = "order_id", value = "订单id")
    private Long orderId;

    @ApiModelProperty(name = "collecting_account_id", value = "收款账户id")
    private Long collectingAccountId;

    @ApiModelProperty(name = "collecting_account_name", value = "收款账户名称")
    private String collectingAccountName;

    @ApiModelProperty(name = "collecting_account_sn", value = "收款账户编号")
    private String collectingAccountSn;

    @ApiModelProperty(name = "price", value = "支付金额")
    private Double price;

    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

}
