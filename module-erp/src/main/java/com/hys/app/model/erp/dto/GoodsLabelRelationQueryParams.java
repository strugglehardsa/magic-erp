package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 商品标签-商品关联表分页查询参数
 *
 * @author 张崧
 * 2024-03-20 16:29:17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "商品标签-商品关联表分页查询参数")
public class GoodsLabelRelationQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "label_id", value = "标签id")
    private Long labelId;
    
    @ApiModelProperty(name = "goods_id", value = "商品id")
    private Long goodsId;
    
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long[] createTime;
    
}

