package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 商品借出单查询参数实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class LendFormQueryParam extends BaseQueryParam implements Serializable {

    private static final long serialVersionUID = -3161414028224314040L;

    @ApiModelProperty(name = "sn", value = "借出单编号")
    private String sn;

    @ApiModelProperty(name = "dept_id", value = "部门ID")
    private Long deptId;

    @ApiModelProperty(name = "start_time", value = "开始时间")
    private Long startTime;

    @ApiModelProperty(name = "end_time", value = "结束时间")
    private Long endTime;

    @ApiModelProperty(name = "warehouse_id", value = "仓库ID")
    private Long warehouseId;

    @ApiModelProperty(name = "status", value = "状态", allowableValues = "NEW,CONFIRMED,RETURNED")
    private String status;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "LendFormQueryParam{" +
                "sn='" + sn + '\'' +
                ", deptId=" + deptId +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", warehouseId='" + warehouseId +
                ", status='" + status + '\'' +
                '}';
    }
}
