package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;

/**
 * 商品标签 DO
 *
 * @author 张崧
 * 2024-03-20 16:23:19
 */
@TableName("erp_goods_label")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class GoodsLabelDO extends BaseDO {

    private static final long serialVersionUID = 1L;
    
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(name = "name", value = "标签名称")
    private String name;
    
}
