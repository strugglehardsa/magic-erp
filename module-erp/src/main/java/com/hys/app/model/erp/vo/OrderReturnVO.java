package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.OrderReturnDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

/**
 * 订单退货VO
 *
 * @author 张崧
 * @since 2023-12-14 15:42:07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class OrderReturnVO extends OrderReturnDO {

    @ApiModelProperty(name = "allowable", value = "允许的操作")
    private OrderReturnAllowable allowable;

    @ApiModelProperty(name = "item_list", value = "退货项列表")
    private List<OrderReturnItemVO> itemList;

}

