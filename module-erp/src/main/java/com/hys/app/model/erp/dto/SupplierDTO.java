package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 供应商新增/编辑
 *
 * @author 张崧
 * 2023-11-29 14:20:18
 */
@Data
@ToString(callSuper = true)
public class SupplierDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "custom_name", value = "客户名称")
    @NotBlank(message = "客户名称不能为空")
    private String customName;

    @ApiModelProperty(name = "transfer", value = "传真")
    private String transfer;

    @ApiModelProperty(name = "zipcode", value = "邮政编码")
    private String zipcode;

    @ApiModelProperty(name = "postal_address", value = "通讯地址")
    private String postalAddress;

    @ApiModelProperty(name = "telephone", value = "联系电话")
    private String telephone;

    @ApiModelProperty(name = "company_website", value = "公司网站")
    private String companyWebsite;

    @ApiModelProperty(name = "linkman", value = "联系人")
    private String linkman;

    @ApiModelProperty(name = "email", value = "电子邮件")
    private String email;

    @ApiModelProperty(name = "mobile", value = "手机号码")
    private String mobile;

    @ApiModelProperty(name = "production_address", value = "生产地址")
    private String productionAddress;

    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;
}

