package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 财务金额类型枚举
 *
 * @author 张崧
 * @since 2024-03-15
 */
@Getter
@AllArgsConstructor
public enum FinanceAmountTypeEnum {

    /**
     * 收入
     */
    Income,
    /**
     * 支出
     */
    Expend,

}
