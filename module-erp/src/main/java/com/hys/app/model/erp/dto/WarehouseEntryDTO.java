package com.hys.app.model.erp.dto;

import com.hys.app.model.erp.dos.ProcurementContract;
import com.hys.app.model.erp.dos.ProcurementContractProduct;
import com.hys.app.model.erp.dos.ProcurementPlan;
import com.hys.app.model.erp.dos.SupplierDO;
import com.hys.app.model.erp.dos.WarehouseDO;
import com.hys.app.model.system.dos.AdminUser;
import com.hys.app.model.system.dos.DeptDO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Map;

/**
 * 入库单新增/编辑DTO
 *
 * @author 张崧
 * @since 2023-12-05 11:25:07
 */
@Data
public class WarehouseEntryDTO {

    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "entry_time", value = "入库时间")
    @NotNull(message = "入库时间不能为空")
    private Long entryTime;
    
    @ApiModelProperty(name = "supplier_id", value = "供应商id")
    @NotNull(message = "供应商不能为空")
    private Long supplierId;
    
    @ApiModelProperty(name = "dept_id", value = "部门id")
    @NotNull(message = "部门不能为空")
    private Long deptId;
    
    @ApiModelProperty(name = "warehouse_id", value = "仓库id")
    @NotNull(message = "仓库不能为空")
    private Long warehouseId;
    
    @ApiModelProperty(name = "contract_id", value = "合同id")
    private Long contractId;
    
    @ApiModelProperty(name = "handled_by_id", value = "经手人id")
    @NotNull(message = "经手人id不能为空")
    private Long handledById;
    
    @ApiModelProperty(name = "plate_number", value = "车牌号")
    private String plateNumber;
    
    @ApiModelProperty(name = "purchase_plan_id", value = "采购计划id")
    private Long purchasePlanId;
    
    @ApiModelProperty(name = "delivery_number", value = "送货单号")
    private String deliveryNumber;
    
    @ApiModelProperty(name = "contract_attachment", value = "合同附件")
    private String contractAttachment;

    @ApiModelProperty(name = "product_list", value = "商品明细列表")
    @NotEmpty(message = "商品明细列表不能为空")
    @Valid
    private List<WarehouseEntryProductDTO> productList;


    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private DeptDO deptDO;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private WarehouseDO warehouseDO;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private SupplierDO supplierDO;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private ProcurementPlan procurementPlan;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private ProcurementContract contract;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private AdminUser handledBy;

    @JsonIgnore
    @ApiModelProperty(hidden = true)
    private Map<Long, ProcurementContractProduct> contractProductMap;

}

