package com.hys.app.model.erp.dto;

import com.hys.app.framework.database.BaseQueryParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 供应商退货项查询参数
 *
 * @author 张崧
 * @since 2023-12-14 11:12:46
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SupplierReturnItemQueryParams extends BaseQueryParam {

    @ApiModelProperty(name = "supplier_return_id", value = "供应商退货id")
    private Long supplierReturnId;
    
    @ApiModelProperty(name = "warehouse_entry_batch_id", value = "批次id")
    private Long warehouseEntryBatchId;
    
    @ApiModelProperty(name = "warehouse_entry_batch_sn", value = "批次编号")
    private String warehouseEntryBatchSn;
    
    @ApiModelProperty(name = "product_id", value = "商品id")
    private Long productId;
    
    @ApiModelProperty(name = "product_sn", value = "商品编号")
    private String productSn;
    
    @ApiModelProperty(name = "product_name", value = "商品名称")
    private String productName;
    
    @ApiModelProperty(name = "product_specification", value = "商品规格")
    private String productSpecification;
    
    @ApiModelProperty(name = "product_price", value = "商品单价")
    private Double productPrice;
    
    @ApiModelProperty(name = "entry_num", value = "入库数量")
    private Integer entryNum;
    
    @ApiModelProperty(name = "return_num", value = "退货数量")
    private Integer returnNum;
    
}

