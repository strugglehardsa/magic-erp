package com.hys.app.model.erp.vo;

import io.swagger.annotations.ApiModel;
import com.hys.app.model.erp.dos.FinanceItemDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 财务明细详情
 *
 * @author 张崧
 * 2024-03-15 17:40:23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "财务明细详情")
public class FinanceItemVO extends FinanceItemDO {

}

