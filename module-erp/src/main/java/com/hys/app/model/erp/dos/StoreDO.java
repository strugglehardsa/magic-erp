package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * 门店实体类
 *
 * @author 张崧
 * @since 2023-12-11 17:47:09
 */
@TableName("erp_store")
@Data
public class StoreDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;

    @ApiModelProperty(name = "store_name", value = "门店名称")
    private String storeName;

    @ApiModelProperty(name = "address", value = "门店地址")
    private String address;

    @ApiModelProperty(name = "contact_person", value = "联系人")
    private String contactPerson;

    @ApiModelProperty(name = "telephone", value = "门店联系电话")
    private String telephone;

    @ApiModelProperty(name = "delete_flag", value = "删除标志 0未删除 1已删除")
    private Integer deleteFlag;

    @ApiModelProperty(name = "create_time", value = "门店创建时间")
    private Long createTime;

}
