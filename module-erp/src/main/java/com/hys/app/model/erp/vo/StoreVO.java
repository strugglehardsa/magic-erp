package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.StoreDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 门店VO
 *
 * @author 张崧
 * @since 2023-12-11 17:47:09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class StoreVO extends StoreDO {

}

