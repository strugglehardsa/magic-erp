package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hys.app.model.erp.vo.ContractAllowable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 采购合同实体
 * @Author dmy
 * 2023-12-05
 */
@TableName("es_procurement_contract")
@ApiModel
public class ProcurementContract implements Serializable {

    private static final long serialVersionUID = -4740017244901559849L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 编号
     */
    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    /**
     * 签订时间
     */
    @ApiModelProperty(name = "sign_time", value = "签订时间")
    private Long signTime;
    /**
     * 供应商ID
     */
    @ApiModelProperty(name = "supplier_id", value = "供应商ID")
    private Long supplierId;
    /**
     * 供应商名称
     */
    @ApiModelProperty(name = "supplier_name", value = "供应商名称")
    private String supplierName;
    /**
     * 制单人ID
     */
    @ApiModelProperty(name = "creator_id", value = "制单人ID")
    private Long creatorId;
    /**
     * 制单人
     */
    @ApiModelProperty(name = "creator", value = "制单人")
    private String creator;
    /**
     * 制单时间
     */
    @ApiModelProperty(name = "creator_time", value = "制单时间")
    private Long creatorTime;
    /**
     * 合同附件
     */
    @ApiModelProperty(name = "contract_annex", value = "合同附件")
    private String contractAnnex;
    /**
     * 说明
     */
    @ApiModelProperty(name = "contract_desc", value = "说明")
    private String contractDesc;
    /**
     * 创建时间
     */
    @ApiModelProperty(name = "create_time", value = "创建时间")
    private Long createTime;
    /**
     * 合同状态
     * @see com.hys.app.model.erp.enums.ContractStatusEnum
     */
    @ApiModelProperty(name = "contract_status", value = "合同状态")
    private String contractStatus;
    /**
     * 是否已删除 0：否，1：是
     */
    @ApiModelProperty(name = "disabled", value = "是否已删除 0：否，1：是")
    private Integer disabled;
    /**
     * 合同允许进行的操作
     */
    @TableField(exist = false)
    @ApiModelProperty(name = "allowable", value = "合同允许进行的操作")
    private ContractAllowable allowable;
    /**
     * 采购计划ID
     */
    @ApiModelProperty(name = "plan_id", value = "采购计划ID")
    private Long planId;
    /**
     * 采购计划编号
     */
    @ApiModelProperty(name = "plan_sn", value = "采购计划编号")
    private String planSn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getSignTime() {
        return signTime;
    }

    public void setSignTime(Long signTime) {
        this.signTime = signTime;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getCreatorTime() {
        return creatorTime;
    }

    public void setCreatorTime(Long creatorTime) {
        this.creatorTime = creatorTime;
    }

    public String getContractAnnex() {
        return contractAnnex;
    }

    public void setContractAnnex(String contractAnnex) {
        this.contractAnnex = contractAnnex;
    }

    public String getContractDesc() {
        return contractDesc;
    }

    public void setContractDesc(String contractDesc) {
        this.contractDesc = contractDesc;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    public ContractAllowable getAllowable() {
        return allowable;
    }

    public void setAllowable(ContractAllowable allowable) {
        this.allowable = allowable;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public String getPlanSn() {
        return planSn;
    }

    public void setPlanSn(String planSn) {
        this.planSn = planSn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProcurementContract that = (ProcurementContract) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(sn, that.sn) &&
                Objects.equals(signTime, that.signTime) &&
                Objects.equals(supplierId, that.supplierId) &&
                Objects.equals(supplierName, that.supplierName) &&
                Objects.equals(creatorId, that.creatorId) &&
                Objects.equals(creator, that.creator) &&
                Objects.equals(creatorTime, that.creatorTime) &&
                Objects.equals(contractAnnex, that.contractAnnex) &&
                Objects.equals(contractDesc, that.contractDesc) &&
                Objects.equals(createTime, that.createTime) &&
                Objects.equals(contractStatus, that.contractStatus) &&
                Objects.equals(disabled, that.disabled) &&
                Objects.equals(allowable, that.allowable) &&
                Objects.equals(planId, that.planId) &&
                Objects.equals(planSn, that.planSn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, sn, signTime, supplierId, supplierName, creatorId, creator, creatorTime, contractAnnex, contractDesc, createTime, contractStatus, disabled, allowable, planId, planSn);
    }

    @Override
    public String toString() {
        return "ProcurementContract{" +
                "id=" + id +
                ", sn='" + sn + '\'' +
                ", signTime=" + signTime +
                ", supplierId=" + supplierId +
                ", supplierName='" + supplierName + '\'' +
                ", creatorId=" + creatorId +
                ", creator='" + creator + '\'' +
                ", creatorTime=" + creatorTime +
                ", contractAnnex='" + contractAnnex + '\'' +
                ", contractDesc='" + contractDesc + '\'' +
                ", createTime=" + createTime +
                ", contractStatus='" + contractStatus + '\'' +
                ", disabled=" + disabled +
                ", allowable=" + allowable +
                ", planId=" + planId +
                ", planSn='" + planSn + '\'' +
                '}';
    }
}
