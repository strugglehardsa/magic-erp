package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.SupplierReturnItemDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 供应商退货项VO
 *
 * @author 张崧
 * @since 2023-12-14 11:12:46
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SupplierReturnItemVO extends SupplierReturnItemDO {

}

