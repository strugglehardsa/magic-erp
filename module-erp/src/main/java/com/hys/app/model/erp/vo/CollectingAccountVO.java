package com.hys.app.model.erp.vo;

import io.swagger.annotations.ApiModel;
import com.hys.app.model.erp.dos.CollectingAccountDO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 收款账户详情
 *
 * @author 张崧
 * 2024-01-24 14:43:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "收款账户详情")
public class CollectingAccountVO extends CollectingAccountDO {

}

