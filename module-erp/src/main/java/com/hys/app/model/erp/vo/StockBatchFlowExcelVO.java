package com.hys.app.model.erp.vo;

import lombok.Data;
import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 库存批次流水excel导出
 *
 * @author 张崧
 * 2024-01-16 11:39:01
 */
@Data
public class StockBatchFlowExcelVO {

    @ExcelProperty("入库单id")
    private Long warehouseEntryId;
    
    @ExcelProperty("入库单编号")
    private String warehouseEntrySn;
    
    @ExcelProperty("批次id")
    private Long batchId;
    
    @ExcelProperty("批次编号")
    private String batchSn;
    
    @ExcelProperty("仓库id")
    private Long warehouseId;
    
    @ExcelProperty("商品id")
    private Long goodsId;
    
    @ExcelProperty("产品id")
    private Long productId;
    
    @ExcelProperty("产品编号")
    private String productSn;
    
    @ExcelProperty("产品名称")
    private String productName;
    
    @ExcelProperty("产品规格")
    private String productSpecification;
    
    @ExcelProperty("产品单位")
    private String productUnit;
    
    @ExcelProperty("产品条形码")
    private String productBarcode;
    
    @ExcelProperty("产品分类id")
    private Long categoryId;
    
    @ExcelProperty("产品分类名称")
    private String categoryName;
    
    @ExcelProperty("产品单价")
    private Double productPrice;
    
    @ExcelProperty("产品进货单价")
    private Double productCostPrice;
    
    @ExcelProperty("税率")
    private Double taxRate;
    
    @ExcelProperty("库存变更数量")
    private Integer changeNum;
    
    @ExcelProperty("批次剩余库存数量")
    private Integer batchRemainNum;
    
    @ExcelProperty("库存变更来源")
    private String sourceType;
    
    @ExcelProperty("库存变更来源单号")
    private String sourceSn;
    
}

