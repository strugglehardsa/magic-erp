package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.ChangeFormProduct;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 换货单商品统计实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class ChangeFormStatistics extends ChangeFormProduct implements Serializable {

    private static final long serialVersionUID = 2258572477723744084L;

    /**
     * 换货单编号
     */
    @ApiModelProperty(name = "sn", value = "换货单编号")
    private String sn;
    /**
     * 换货时间
     */
    @ApiModelProperty(name = "change_time", value = "换货时间")
    private Long changeTime;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public Long getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Long changeTime) {
        this.changeTime = changeTime;
    }

    @Override
    public String toString() {
        return "ChangeFormStatistics{" +
                "sn='" + sn + '\'' +
                ", changeTime=" + changeTime +
                '}';
    }
}
