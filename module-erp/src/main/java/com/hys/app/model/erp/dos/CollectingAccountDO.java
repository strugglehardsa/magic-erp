package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import com.hys.app.framework.database.mybatisplus.base.BaseDO;

/**
 * 收款账户 DO
 *
 * @author 张崧
 * 2024-01-24 14:43:17
 */
@TableName("erp_collecting_account")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CollectingAccountDO extends BaseDO {

    private static final long serialVersionUID = 1L;
    
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty(name = "name", value = "账户名称")
    private String name;
    
    @ApiModelProperty(name = "sn", value = "编号")
    private String sn;
    
    @ApiModelProperty(name = "default_flag", value = "是否默认")
    private Boolean defaultFlag;
    
    @ApiModelProperty(name = "enable_flag", value = "是否启用")
    private Boolean enableFlag;
    
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;
    
}
