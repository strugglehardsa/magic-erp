package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * 采购合同实体DTO
 * @Author dmy
 * 2023-12-05
 */
@ApiModel
public class ProcurementContractDTO implements Serializable {

    private static final long serialVersionUID = 3234772358430127326L;

    /**
     * 签订时间
     */
    @ApiModelProperty(name = "sign_time", value = "签订时间", required = true)
    @NotNull(message = "签订时间不能为空")
    private Long signTime;
    /**
     * 供应商ID
     */
    @ApiModelProperty(name = "supplier_id", value = "供应商ID", required = true)
    @NotNull(message = "供应商ID不能为空")
    private Long supplierId;
    /**
     * 供应商名称
     */
    @ApiModelProperty(name = "supplier_name", value = "供应商名称", required = true)
    @NotEmpty(message = "供应商名称不能为空")
    private String supplierName;
    /**
     * 制单人ID
     */
    @ApiModelProperty(name = "creator_id", value = "制单人ID", required = true)
    @NotNull(message = "制单人ID不能为空")
    private Long creatorId;
    /**
     * 制单人
     */
    @ApiModelProperty(name = "creator", value = "制单人", required = true)
    @NotEmpty(message = "制单人不能为空")
    private String creator;
    /**
     * 制单时间
     */
    @ApiModelProperty(name = "creator_time", value = "制单时间", required = true)
    @NotNull(message = "制单时间不能为空")
    private Long creatorTime;
    /**
     * 合同附件
     */
    @ApiModelProperty(name = "contract_annex", value = "合同附件", required = true)
    @NotEmpty(message = "合同附件不能为空")
    private String contractAnnex;
    /**
     * 说明
     */
    @ApiModelProperty(name = "contract_desc", value = "说明")
    private String contractDesc;
    /**
     * 采购合同产品
     */
    @ApiModelProperty(name = "product_list", value = "采购合同产品", required = true)
    @NotNull(message = "采购合同产品不能为空")
    private List<ProcurementContractProductDTO> productList;
    /**
     * 采购计划ID
     */
    @ApiModelProperty(name = "plan_id", value = "采购计划ID")
    @NotNull(message = "采购计划ID不能为空")
    private Long planId;
    /**
     * 采购计划编号
     */
    @ApiModelProperty(name = "plan_sn", value = "采购计划编号")
    @NotEmpty(message = "采购计划编号不能为空")
    private String planSn;

    public Long getSignTime() {
        return signTime;
    }

    public void setSignTime(Long signTime) {
        this.signTime = signTime;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getCreatorTime() {
        return creatorTime;
    }

    public void setCreatorTime(Long creatorTime) {
        this.creatorTime = creatorTime;
    }

    public String getContractAnnex() {
        return contractAnnex;
    }

    public void setContractAnnex(String contractAnnex) {
        this.contractAnnex = contractAnnex;
    }

    public String getContractDesc() {
        return contractDesc;
    }

    public void setContractDesc(String contractDesc) {
        this.contractDesc = contractDesc;
    }

    public List<ProcurementContractProductDTO> getProductList() {
        return productList;
    }

    public void setProductList(List<ProcurementContractProductDTO> productList) {
        this.productList = productList;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public String getPlanSn() {
        return planSn;
    }

    public void setPlanSn(String planSn) {
        this.planSn = planSn;
    }

    @Override
    public String toString() {
        return "ProcurementContractDTO{" +
                "signTime=" + signTime +
                ", supplierId=" + supplierId +
                ", supplierName='" + supplierName + '\'' +
                ", creatorId=" + creatorId +
                ", creator='" + creator + '\'' +
                ", creatorTime=" + creatorTime +
                ", contractAnnex='" + contractAnnex + '\'' +
                ", contractDesc='" + contractDesc + '\'' +
                ", productList=" + productList +
                ", planId=" + planId +
                ", planSn='" + planSn + '\'' +
                '}';
    }
}
