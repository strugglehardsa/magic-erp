package com.hys.app.model.erp.vo;

import com.hys.app.model.erp.dos.OrderDO;
import com.hys.app.model.erp.enums.OrderPaymentStatusEnum;
import com.hys.app.model.erp.enums.OrderStatusEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 订单允许的操作
 *
 * @author 张崧
 * @since 2024-01-25
 */
@Data
@NoArgsConstructor
public class OrderAllowable {

    @ApiModelProperty(name = "edit", value = "是否允许编辑")
    private Boolean edit;

    @ApiModelProperty(name = "submit", value = "是否允许提交")
    private Boolean submit;

    @ApiModelProperty(name = "withdraw", value = "是否允许撤回")
    private Boolean withdraw;

    @ApiModelProperty(name = "audit", value = "是否允许审核")
    private Boolean audit;

    @ApiModelProperty(name = "delete", value = "是否允许删除")
    private Boolean delete;

    @ApiModelProperty(name = "warehouse_out", value = "是否允许出库")
    private Boolean warehouseOut;

    @ApiModelProperty(name = "payment", value = "是否允许支付")
    private Boolean payment;

    public OrderAllowable(OrderDO orderDO) {
        OrderStatusEnum status = orderDO.getStatus();
        if (status == null) {
            return;
        }

        this.setEdit(status == OrderStatusEnum.WAIT_SUBMIT ||
                status == OrderStatusEnum.AUDIT_REJECT);

        this.setSubmit(status == OrderStatusEnum.WAIT_SUBMIT ||
                status == OrderStatusEnum.AUDIT_REJECT);

        this.setWithdraw(status == OrderStatusEnum.WAIT_AUDIT);

        this.setAudit(status == OrderStatusEnum.WAIT_AUDIT);

        this.setDelete(status == OrderStatusEnum.WAIT_SUBMIT ||
                status == OrderStatusEnum.WAIT_AUDIT ||
                status == OrderStatusEnum.AUDIT_REJECT);

        this.setWarehouseOut(status == OrderStatusEnum.WAIT_WAREHOUSE_OUT);

        this.setPayment(orderDO.getPaymentStatus() == OrderPaymentStatusEnum.NOT_PAY);
    }

}

