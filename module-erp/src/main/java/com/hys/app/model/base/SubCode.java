package com.hys.app.model.base;

/**
 * 子业务代码
 *
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2020/4/17
 */

public class SubCode {

    // 凭证
    public static final int Voucher = 11;

}
