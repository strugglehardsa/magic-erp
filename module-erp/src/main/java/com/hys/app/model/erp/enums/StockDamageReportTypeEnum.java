package com.hys.app.model.erp.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 库存报损类型
 *
 * @author 张崧
 * @since 2024-01-09
 */
@Getter
@AllArgsConstructor
public enum StockDamageReportTypeEnum {

    /**
     * 库存损耗
     */
    StockDamage("库存损耗"),
    /**
     * 销售活动
     */
    SaleActivity("销售活动");

    private final String description;
}
