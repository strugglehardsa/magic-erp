package com.hys.app.model.system.dto;

import com.hys.app.model.system.dos.AdminUser;
import io.swagger.annotations.ApiModelProperty;

/**
 * 管理员对象 用于管理员列表显示
 *
 * @author zh
 * @version v7.0
 * @date 18/6/27 下午2:42
 * @since v7.0
 */

public class AdminUserDTO extends AdminUser {

    @ApiModelProperty(name = "role_name", value = "角色名称", required = false)
    private String roleName;

    @ApiModelProperty(name = "dept_name", value = "部门名称", required = false)
    private String deptName;

    @ApiModelProperty(name = "post_name", value = "岗位名称", required = false)
    private String postName;

    public String getRoleName() {
        if (this.getFounder().equals(1)) {
            roleName = "超级管理员";
        }
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    @Override
    public String toString() {
        return "AdminUserDTO{" +
                "roleName='" + roleName + '\'' +
                '}';
    }
}
