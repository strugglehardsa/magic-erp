package com.hys.app.model.erp.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Objects;

/**
 * 采购计划商品实体
 * @Author dmy
 * 2023-12-05
 */
@TableName("es_procurement_plan_product")
@ApiModel
public class ProcurementPlanProduct implements Serializable {

    private static final long serialVersionUID = -4464982719968269923L;

    /**
     * 主键ID
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(hidden = true)
    private Long id;
    /**
     * 采购计划ID
     */
    @ApiModelProperty(name = "plan_id", value = "采购计划ID")
    private Long planId;
    /**
     * 商品ID
     */
    @ApiModelProperty(name = "goods_id", value = "商品ID")
    private Long goodsId;
    /**
     * 产品ID
     */
    @ApiModelProperty(name = "product_id", value = "产品ID")
    private Long productId;
    /**
     * 产品名称
     */
    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;
    /**
     * 产品编号
     */
    @ApiModelProperty(name = "product_sn", value = "产品编号")
    private String productSn;
    /**
     * 产品规格
     */
    @ApiModelProperty(name = "specification", value = "产品规格")
    private String specification;
    /**
     * 分类id
     */
    @ApiModelProperty(name = "category_id", value = "分类id")
    private Long categoryId;
    /**
     * 分类名称
     */
    @ApiModelProperty(name = "category_name", value = "分类名称")
    private String categoryName;
    /**
     * 产品单位
     */
    @ApiModelProperty(name = "unit", value = "产品单位")
    private String unit;
    /**
     * 条形码
     */
    @ApiModelProperty(name = "barcode", value = "条形码")
    private String barcode;
    /**
     * 计划采购数量
     */
    @ApiModelProperty(name = "procurement_num", value = "计划采购数量")
    private Integer procurementNum;
    /**
     * 计划供应时间
     */
    @ApiModelProperty(name = "supply_time", value = "计划供应时间")
    private Long supplyTime;
    /**
     * 备注
     */
    @ApiModelProperty(name = "remark", value = "备注")
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductSn() {
        return productSn;
    }

    public void setProductSn(String productSn) {
        this.productSn = productSn;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Integer getProcurementNum() {
        return procurementNum;
    }

    public void setProcurementNum(Integer procurementNum) {
        this.procurementNum = procurementNum;
    }

    public Long getSupplyTime() {
        return supplyTime;
    }

    public void setSupplyTime(Long supplyTime) {
        this.supplyTime = supplyTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProcurementPlanProduct goods = (ProcurementPlanProduct) o;
        return Objects.equals(id, goods.id) &&
                Objects.equals(planId, goods.planId) &&
                Objects.equals(goodsId, goods.goodsId) &&
                Objects.equals(productId, goods.productId) &&
                Objects.equals(productName, goods.productName) &&
                Objects.equals(productSn, goods.productSn) &&
                Objects.equals(specification, goods.specification) &&
                Objects.equals(categoryId, goods.categoryId) &&
                Objects.equals(categoryName, goods.categoryName) &&
                Objects.equals(unit, goods.unit) &&
                Objects.equals(barcode, goods.barcode) &&
                Objects.equals(procurementNum, goods.procurementNum) &&
                Objects.equals(supplyTime, goods.supplyTime) &&
                Objects.equals(remark, goods.remark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, planId, goodsId, productId, productName, productSn, specification, categoryId, categoryName, unit, barcode, procurementNum, supplyTime, remark);
    }

    @Override
    public String toString() {
        return "ProcurementPlanProduct{" +
                "id=" + id +
                ", planId=" + planId +
                ", goodsId=" + goodsId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productSn='" + productSn + '\'' +
                ", specification='" + specification + '\'' +
                ", categoryId=" + categoryId +
                ", categoryName='" + categoryName + '\'' +
                ", unit='" + unit + '\'' +
                ", barcode='" + barcode + '\'' +
                ", procurementNum=" + procurementNum +
                ", supplyTime=" + supplyTime +
                ", remark='" + remark + '\'' +
                '}';
    }
}
