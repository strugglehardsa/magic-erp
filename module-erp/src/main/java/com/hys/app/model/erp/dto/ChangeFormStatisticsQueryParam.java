package com.hys.app.model.erp.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 换货单产品统计查询参数实体
 *
 * @author dmy
 * 2023-12-05
 */
@ApiModel
public class ChangeFormStatisticsQueryParam extends ChangeFormQueryParam implements Serializable {

    private static final long serialVersionUID = -326297890277132550L;

    @ApiModelProperty(name = "warehouse_id", value = "仓库ID")
    private Long warehouseId;

    @ApiModelProperty(name = "product_name", value = "产品名称")
    private String productName;

    @ApiModelProperty(name = "category_id", value = "分类ID")
    private Long categoryId;

    @ApiModelProperty(name = "type", value = "调换类型 0：退货，1：换货", allowableValues = "0,1")
    private Integer type;

    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ChangeFormStatisticsQueryParam{" +
                "warehouseId=" + warehouseId +
                ", productName='" + productName + '\'' +
                ", categoryId=" + categoryId +
                ", type=" + type +
                '}';
    }
}
