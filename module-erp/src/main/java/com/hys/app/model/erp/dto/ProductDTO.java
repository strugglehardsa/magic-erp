package com.hys.app.model.erp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hys.app.model.erp.dos.ProductDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 产品新增/编辑DTO
 *
 * @author 张崧
 * 2023-11-30 16:06:44
 */
@Data
public class ProductDTO {

    @ApiModelProperty(hidden = true)
    @JsonIgnore
    private Long id;

    @ApiModelProperty(name = "sn", value = "编号（如果是无规格，直接使用商品的编号）")
    private String sn;

    @ApiModelProperty(name = "price", value = "销售价格")
    @NotNull(message = "销售价格不能为空")
    @Min(value = 0, message = "销售价格不能小于0")
    private Double price;

    @ApiModelProperty(name = "cost_price", value = "成本价格")
    @NotNull(message = "成本价格不能为空")
    @Min(value = 0, message = "成本价格不能小于0")
    private Double costPrice;

    @ApiModelProperty(name = "retail_price", value = "零售价格")
    @NotNull(message = "零售价格不能为空")
    @Min(value = 0, message = "零售价格不能小于0")
    private Double retailPrice;

    @ApiModelProperty(name = "tax_rate", value = "税率")
    @NotNull(message = "税率不能为空")
    @Min(value = 0, message = "税率不能小于0")
    private Double taxRate;

    @ApiModelProperty(name = "mkt_price", value = "市场价格（暂时保留）", hidden = true)
    @Min(value = 0, message = "市场价格不能小于0")
    private Double mktPrice;

    @ApiModelProperty(name = "weight", value = "重量")
    @NotNull(message = "重量不能为空")
    @Min(value = 0, message = "重量不能小于0")
    private Double weight;

    @ApiModelProperty(name = "unit", value = "单位")
    @NotBlank(message = "单位不能为空")
    private String unit;

    @ApiModelProperty(name = "warning_value", value = "库存预警数")
    @NotNull(message = "库存预警数不能为空")
    private Integer warningValue;

    @ApiModelProperty(name = "spec_list", value = "规格列表")
    private ProductDO.SpecList specList;

}

