package com.hys.app.model.erp.vo;

import io.swagger.annotations.ApiModel;
import com.hys.app.model.erp.dos.OrderItemDO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 订单明细详情
 *
 * @author 张崧
 * 2024-01-24 16:39:38
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@ApiModel(value = "订单明细详情")
public class OrderItemVO extends OrderItemDO {

    @ApiModelProperty(name = "stock_num", value = "库存数量")
    private Integer stockNum;
}

