package com.hys.app.mq.event;

import com.hys.app.model.erp.dto.message.GoodsChangeMessage;

/**
 * 商品变更事件
 *
 * @author 张崧
 * @since 2024-01-19
 */
public interface GoodsChangeEvent {

    /**
     * 商品变更
     *
     * @param message 商品消息
     */
    void onGoodsChange(GoodsChangeMessage message);

}
