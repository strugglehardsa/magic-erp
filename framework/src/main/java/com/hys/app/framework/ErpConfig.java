package com.hys.app.framework;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 配置
 *
 * @author zh
 * @version v7.0
 * @date 18/4/13 下午8:19
 * @since v7.0
 */
@Configuration
@ConfigurationProperties(prefix = "erp")
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@SuppressWarnings("ConfigurationProperties")
public class ErpConfig {

    /**
     * token加密秘钥
     */
    private String tokenSecret;

    /**
     * 访问token失效时间
     */
    @Value("${erp.timeout.accessTokenTimeout:#{null}}")
    private Integer accessTokenTimeout;

    @Value("${erp.timeout.refreshTokenTimeout:#{null}}")
    private Integer refreshTokenTimeout;

    @Value("${erp.timeout.captchaTimout:#{null}}")
    private Integer captchaTimout;

    @Value("${erp.timeout.smscodeTimout:#{null}}")
    private Integer smscodeTimout;

    @Value("${erp.debugger:#{false}}")
    private boolean debugger;


    public ErpConfig() {
    }


    @Override
    public String toString() {
        return "erpConfig{" +
                "accessTokenTimeout=" + accessTokenTimeout +
                ", refreshTokenTimeout=" + refreshTokenTimeout +
                ", captchaTimout=" + captchaTimout +
                ", smscodeTimout=" + smscodeTimout +
                '}';
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }

    public Integer getAccessTokenTimeout() {
        return accessTokenTimeout;
    }

    public void setAccessTokenTimeout(Integer accessTokenTimeout) {
        this.accessTokenTimeout = accessTokenTimeout;
    }

    public Integer getRefreshTokenTimeout() {
        return refreshTokenTimeout;
    }

    public void setRefreshTokenTimeout(Integer refreshTokenTimeout) {
        this.refreshTokenTimeout = refreshTokenTimeout;
    }

    public Integer getCaptchaTimout() {
        return captchaTimout;
    }

    public void setCaptchaTimout(Integer captchaTimout) {
        this.captchaTimout = captchaTimout;
    }

    public Integer getSmscodeTimout() {
        return smscodeTimout;
    }

    public void setSmscodeTimout(Integer smscodeTimout) {
        this.smscodeTimout = smscodeTimout;
    }

    public boolean isDebugger() {
        return debugger;
    }

    public void setDebugger(boolean debugger) {
        this.debugger = debugger;
    }

}
