package com.hys.app.framework.trigger.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 路由配置=
 *
 * @author liushuai
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2019/2/12 下午3:25
 */

@Configuration
public class TimeTriggerConfig {

    /**
     * 队列枚举
     */
    public final static String IMMEDIATE_QUEUE_XDELAY = "IMMEDIATE_QUEUE_XDELAY";
    /**
     * 交换机
     */
    public final static String DELAYED_EXCHANGE_XDELAY = "DELAYED_EXCHANGE_XDELAY";
    /**
     * routing
     */
    public final static String DELAY_ROUTING_KEY_XDELAY = "DELAY_ROUTING_KEY_XDELAY";

    /**
     * chain队列枚举
     */
    public final static String CHAIN_QUEUE_XDELAY = "CHAIN_QUEUE_XDELAY";

    /**
     * chain-routing
     */
    public final static String CHAIN_ROUTING_KEY_XDELAY = "CHAIN_ROUTING_KEY_XDELAY";


    @Bean(name = DELAYED_EXCHANGE_XDELAY)
    public CustomExchange delayExchange() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        return new CustomExchange(DELAYED_EXCHANGE_XDELAY, "x-delayed-message", true, false, args);
    }

    @Bean(name = IMMEDIATE_QUEUE_XDELAY)
    public Queue queue() {
        Queue queue = new Queue(IMMEDIATE_QUEUE_XDELAY, true);
        return queue;
    }

    @Bean
    public Binding binding() {
        return BindingBuilder.bind(queue()).to(delayExchange()).with(DELAY_ROUTING_KEY_XDELAY).noargs();
    }

    @Bean(name = CHAIN_QUEUE_XDELAY)
    public Queue chainQueue() {
        Queue queue = new Queue(CHAIN_QUEUE_XDELAY, true);
        return queue;
    }

    @Bean
    public Binding chainBinding() {
        return BindingBuilder.bind(chainQueue()).to(delayExchange()).with(CHAIN_ROUTING_KEY_XDELAY).noargs();
    }
}