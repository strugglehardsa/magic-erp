package com.hys.app.framework.database;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

/**
 * 基础查询参数
 *
 * @author cs
 * @since v7.3.0
 * 2022/10/20
 */
public class BaseQueryParam {

    /**
     * 不分页
     */
    public static final long PAGE_NONE = -1L;

    /**
     * 页码
     */
    @ApiModelProperty(name = "page_no", value = "页码")
    @NotNull(message = "页码不能为空")
    private Long pageNo = 1L;

    /**
     * 页大小
     */
    @ApiModelProperty(name = "page_size", value = "页大小")
    @NotNull(message = "页大小不能为空")
    private Long pageSize = 10L;

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }
}
