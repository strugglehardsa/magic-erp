package com.hys.app.framework.ws;

import com.hys.app.framework.ws.impl.WebSocketHandler;
import com.hys.app.framework.ws.impl.WebSocketInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import javax.annotation.Resource;

/**
 * 配置websocket的Hander以及拦截器
 * Author: Dawei
 * Datetime: 2022-07-17 15:58
 */
@EnableWebSocket
@Configuration
public class WebSocketConfig implements WebSocketConfigurer {

    @Resource
    private WebSocketHandler imWebSocketHandler;

    @Resource
    private WebSocketInterceptor imWebSocketInterceptor;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(imWebSocketHandler, "/ws/shop")
                .addInterceptors(imWebSocketInterceptor).setAllowedOrigins("*");
    }
}