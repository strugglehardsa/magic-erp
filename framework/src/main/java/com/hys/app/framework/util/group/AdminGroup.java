package com.hys.app.framework.util.group;

/**
 * @author gy
 * @version 1.0
 * @Description: 会员端分组
 * @date 2021年7月16日10:54:40
 * @since v7.0
 */
public interface AdminGroup extends BaseGroup{
}
