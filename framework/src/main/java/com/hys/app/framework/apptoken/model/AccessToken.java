package com.hys.app.framework.apptoken.model;

import com.hys.app.framework.util.DateUtil;

import java.io.Serializable;

/**
 * 第三方应用 accessToken
 * @author kingapex
 * @version 1.0
 * @data 2021/8/22 16:15
 **/
public class AccessToken implements Serializable {
    private static final long serialVersionUID = 9115135330405642L;

    public AccessToken(String token, Long expires) {
        this.token = token;
        this.expires = expires;
    }

    /**
     * token值
     */
    private String token;

    /**
     * 有效期
     */
    private  Long expires;

    public String getToken() {
        return token;
    }

    public Long getExpires() {
        return expires;
    }

    @Override
    public String toString() {
        long now = DateUtil.getDateline();
        boolean isEnable = now<=expires;

        return "AccessToken{" +
                "token='" + token + '\'' +
                ", expires=" + expires +
                ",isEnable="+isEnable+
                '}';
    }
}
