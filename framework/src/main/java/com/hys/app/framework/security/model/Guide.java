package com.hys.app.framework.security.model;

/**
 * 导购
 *
 * @author 张崧
 * @version v7.0
 * @date 2021-08-26
 * @since v7.0
 */

public class Guide extends Seller {

    /**
     * 导购id
     */
    private Long guideId;
    /**
     * 导购名称
     */
    private String guideName;


    public Guide() {
        
    }

    public Long getGuideId() {
        return guideId;
    }

    public void setGuideId(Long guideId) {
        this.guideId = guideId;
    }

    public String getGuideName() {
        return guideName;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }
}
