package com.hys.app.framework.ws.model;

import com.hys.app.framework.context.app.AppTypeEnum;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 *
 * @description: 消息模型
 * @author: liuyulei
 * @create: 2021/3/31  9:29
 * @version:1.0
 * @since:7.3.0
 **/
@ApiModel
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Message implements Serializable {
    private static final long serialVersionUID = 9015550663969917072L;

    @ApiModelProperty(value = "端类型", required = true)
    private AppTypeEnum appType;

    @ApiModelProperty(value = "模块类型", required = true)
    private String moduleType;

    /**
     * 用户ID
     */
    @NotEmpty(message = "用户ID不能为空")
    @ApiModelProperty(value = "用户ID", required = true)
    private Long userId;



    /**
     * 消息内容
     */
    @NotEmpty(message = "业务数据不能为空")
    @ApiModelProperty(value = "业务数据", required = true)
    private  Object data;


    /**
     * 消息发送时间
     */
    @ApiModelProperty(hidden = true)
    private Long msgTime;

    public AppTypeEnum getAppType() {
        return appType;
    }

    public void setAppType(AppTypeEnum appType) {
        this.appType = appType;
    }

    public Message() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Long getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(Long msgTime) {
        this.msgTime = msgTime;
    }

    public String getModuleType() {
        return moduleType;
    }

    public void setModuleType(String moduleType) {
        this.moduleType = moduleType;
    }

    @Override
    public String toString() {
        return "Message{" +
                "appType=" + appType +
                ", userId=" + userId +
                ", msg='" + data + '\'' +
                ", msgTime=" + msgTime +
                '}';
    }
}
